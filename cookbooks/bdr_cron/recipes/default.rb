#
# Cookbook Name:: bdr_cron
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# Resources used
# https://github.com/chef-cookbooks/windows


windows_task 'Chef Service Watcher' do
  run_level :highest
  frequency :hourly
  command "net start chef-client"
end


git_repo_exists = ::Dir.exists?('C:\\chef\\repo\\.git')
windows_task 'Chef Git Update' do
  run_level :highest
  frequency :hourly
  start_time '12:00'
  command "chef-client --once -z C:\\chef\\repo\\update_repo.rb"
  only_if {git_repo_exists}
end


# setup ImageVerification task
windows_task 'Image Verification' do
  run_level :highest
  frequency :daily
  start_time '18:00'
  command "powershell -ExecutionPolicy Bypass -file C:\\installers\\IVwrapper.ps1"
end

# setup monitor script task
windows_task 'PSMON' do
  run_level :highest
  frequency :daily
  start_time '02:00'
  command "powershell -ExecutionPolicy Bypass -file C:\\installers\\Monitor.ps1"
end


#===============================================================#
# Failsafe Task

# put failsafe.rb from cookbook files onto system
cookbook_file 'C:\\temp\\failsafe.rb' do
  source 'failsafe.rb'
end

# setup failsafe download task
windows_task 'ChefFailsafeDownload' do
  run_level :highest
  frequency :daily
  start_time '03:00'
  command "chef-client --once -z C:\\temp\\failsafe.rb"
end

# setup failsafe run of downloaded recipe
windows_task 'ChefFailsafeRun' do
  run_level :highest
  frequency :daily
  start_time '04:00'
  command "chef-client --once -z C:\\temp\\failsafe_run.rb"
end
#===============================================================#

windows_task 'BDRUpdateLegacy' do
  run_level :highest
  frequency :daily
  start_time '01:00'
  command "powershell -ExecutionPolicy Bypass -file C:\\installers\\update.ps1"
end


windows_task 'CentralPointAgentScheduler' do
  run_level :highest
  frequency :daily
  start_time '00:15'
  command "'C:\\Program Files\\CharTec\\pyagent\\pyagent.exe' --helper pyupdate_update & 'C:\\Program Files\\CharTec\\pyupdate.exe'"
end

windows_task 'CentralPointAgentBackupCollector' do
  run_level :highest
  frequency :minute
  frequency_modifier 30
  command "'C:\\Program Files\\CharTec\\pyagent\\pyagent.exe' --im all_backups"
end

windows_task 'CentralPointAgentTestCollector' do
  run_level :highest
  frequency :hourly
  frequency_modifier 6
  command "'C:\\Program Files\\CharTec\\pyagent\\pyagent.exe' --helper classic_test_restore"
end

# setup collector restart task
# removed on 2017-06-07 due to possible issues caused
# windows_task 'CollectorRestart' do # not sure if we still need this, but here it is.. --ZB
#   run_level :highest
#   frequency :daily
#   start_time '02:30'
#   command "powershell -ExecutionPolicy Bypass -file C:\\installers\\collectorrestart.ps1 -InputFormat None"
# end


# array of strings representing task names to be removed
tasks_to_remove = [
  "First Run",
  "SaaS BDR First Run",
  "CollectorRestart" # see comment on resource
]

# remove unused tasks
tasks_to_remove.each do |t|
  windows_task t do
    action :delete
  end
end
