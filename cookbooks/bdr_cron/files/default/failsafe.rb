
# this recipe will run independently of the normal Chef cycle and provide a failsafe for if the Chef server explodes

ftp_url = 'ftp://chartecftp:Wow%405449@ftp.chartec.net/Roger'

remote_file 'C:\\temp\\failsafe_run.rb' do
  source "#{ftp_url}/Chef/failsafe_run.rb"
  retries 2
  ignore_failure true
end

# this file is run periodically by a cron created by the main cookbook
