#
# Cookbook Name:: bdr_dirs_files
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# Resources used
# https://docs.chef.io/resource_directory.html
# https://docs.chef.io/resource_file.html
# https://docs.chef.io/resource_remote_file.html

f_vol_exists = ::Dir.exists?('F:\\')
d_vol_exists = ::Dir.exists?('D:\\')

# BDR Directories
directory 'C:\\BDR'
directory 'C:\\BDR\\ImageVerification'
directory 'C:\\BDR\\Collector'
directory 'C:\\BDR\\Production'
directory 'C:\\BDR\\Production\\DecryptKey' do
  ignore_failure true #in some cases, the production folder may have been created as a file by a bug in the old update script causing this call to fail
end
directory 'C:\\BDR\\Logs'
directory 'C:\\BDR\\Logs\\EventViewer'
directory 'C:\\BDR\\Logs\\hsr'
directory 'C:\\BDR\\HSR'
directory 'C:\\BDR\\scripts'
directory 'C:\\BDR\\server'

directory 'C:\\installers'
directory 'C:\\installers\\prerelease'
directory 'C:\\Temp'


directory 'F:\\Backup' do
  ignore_failure true
  only_if {f_vol_exists}
end
directory 'F:\\Archive' do
  ignore_failure true
  only_if {f_vol_exists}
end
directory 'D:\\Archive' do
  ignore_failure true
  only_if {d_vol_exists}
end
directory 'F:\\DoyenzWorkSpace' do
  ignore_failure true
  only_if {f_vol_exists}
end
directory 'D:\\DoyenzWorkSpace' do
  ignore_failure true
  only_if {d_vol_exists}
end
directory 'F:\\Virtualization' do
  ignore_failure true
  only_if {f_vol_exists}
end
directory 'D:\\Virtualization' do
  ignore_failure true
  only_if {d_vol_exists}
end



# BDR file downloads
ftp_url = 'ftp://chartecftp:Wow%405449@ftp.chartec.net/Roger'
cp_url = "https://centralpoint.chartec.net/downloads/chef_data"

# NOTE: checksums MUST be lowercase SHA-256

# old agent
# remote_file 'C:\\installers\\uAgent.exe' do
#   source "#{ftp_url}/Utilities/uAgent.exe"
#   action :create_if_missing
# end

# stopped in favor of version 4.3.12
# remote_file 'C:\\installers\\VirtualBox4.3.2.zip' do
#   source "#{ftp_url}/VirtualBoot/VirtualBox4.3.2/VirtualBox4.3.2.zip"
#   action :create_if_missing
# end

remote_file 'C:\\installers\\VirtualBox-4-3-12.exe' do
  source "http://download.virtualbox.org/virtualbox/4.3.12/VirtualBox-4.3.12-93733-Win.exe"
  action :create_if_missing
  ignore_failure true
end

remote_file 'C:\\installers\\ShadowProtect_RE_5.0.4.iso' do
  source "#{ftp_url}/StorageCraft_RecoveryEnvironment/ShadowProtect_RE_5.0.4.iso"
  checksum "73ba9ffeb11b5dcc2f3f728e757e67f6c598d883598962a7a573224ead358cbc"
  action :create_if_missing
  ignore_failure true
end

remote_file 'C:\\installers\\ShadowProtect_MSP_5.2.7.exe' do
  source "#{ftp_url}/ShadowProtectSetup_MSP_5.2.7.exe"
  checksum "bc6b86eab58706934dbb28882fd7a8114d6dc13e1c0d7002b27016e2d6242586"
  action :create_if_missing
  retries 2
  ignore_failure true
end

# SPX Prerelease for Creators Update
remote_file 'C:\\installers\\prerelease\\SPX-Install_6.7.0-13_x64.msi' do
  source "#{ftp_url}/ShadowProtect/SPX/RC/SPX-Install_6.7.0-13_x64.msi"
  checksum "844a492674455218d7a815195b665220e34046a0b0b7b714af8045cf2f39eb27"
  action :create_if_missing
  retries 2
  ignore_failure true
end
remote_file 'C:\\installers\\prerelease\\SPXHyperVPlugin-1.0.9-165.msi' do
  source "#{ftp_url}/ShadowProtect/SPX/RC/SPXHyperVPlugin-1.0.9-165.msi"
  checksum "4e7c0a658256f30e96923a69ccedcf2a031942387ac0b8a1c2964130a275afe5"
  action :create_if_missing
  retries 2
  ignore_failure true
end

remote_file 'C:\\installers\\ImageManager_Setup_7.1.0.exe' do
  source "#{ftp_url}/ImageManager/ImageManager_Setup_7.1.0.exe"
  action :create_if_missing
  ignore_failure true
end

remote_file "C:\\Temp\\SophosInstall_Server.exe" do
  source "#{cp_url}/sophos/SophosInstall_Server.exe"
  checksum "ce7cb8d02f2d6dc61aea2d9d35ca8821f9dafd74416e8300d5d076a36b1d6cc5"
  action :create_if_missing
  retries 2
  ignore_failure true
end

remote_file "C:\\Temp\\SophosInstall_Endpt.exe" do
  source "#{cp_url}/sophos/SophosInstall_Endpt.exe"
  checksum '2e8e210eda3ace437a3508891451ad0d1d8cb5433e325faf06340d48e2f91153'
  action :create_if_missing
  retries 2
  ignore_failure true
end

# remote_file 'C:\\installers\\DoyenzAgentInstaller.exe' do
#   source "#{ftp_url}/DoyenzAgentInstaller.exe"
#   action :create_if_missing
# end

remote_file 'C:\\installers\\VSSFix.bat' do
  source "#{ftp_url}/Scripts/VSSFix.bat"
  action :create_if_missing
  ignore_failure true
end

remote_file 'C:\\installers\\IVwrapper.ps1' do
  source "#{ftp_url}/IVwrapper.ps1"
  action :create_if_missing
  ignore_failure true
end

remote_file 'C:\\installers\\Monitor.ps1' do
  source "#{ftp_url}/Scripts/Monitor.ps1"
  action :create
  ignore_failure true
end

# removed on 2017-06-07 due to possible issues caused
# remote_file 'C:\\installers\\collectorrestart.ps1' do # not sure if we still need this, but here it is.. --ZB
#   source "#{ftp_url}/Scripts/collectorrestart.ps1"
#   action :create_if_missing
# end

remote_file 'C:\\installers\\Reset-IM-1-1.bat' do
  source "#{ftp_url}/Scripts/Reset-IM-1-1.bat"
  action :create_if_missing
  ignore_failure true
end

remote_file 'C:\\BDR\\Production\\DecryptKey\\KeyDecryptGUI.exe' do
  source "#{ftp_url}/DecryptKey/KeyDecryptGUI.exe"
  checksum "556b72e997587b389abb3e669ce1d0df50521bd017f7a472718a3a8b1d702371"
  action :create_if_missing
  ignore_failure true #continue on in case the folder doesn't exist (see directory resource for DecryptKey folder)
end

#old update script (just in case)
remote_file 'C:\\installers\\update.ps1' do
  source "#{ftp_url}/update.ps1"
  action :create_if_missing
  ignore_failure true
end



# BDR cleanup files -- REMEMBER TO ESCAPE BACKSLASHES
files_to_cleanup = [
  "C:\\BDR\\Plainkey.txt",
  "C:\\Installers\\ImageManager5.0.5.exe",
  "C:\\Installers\\ImageManager4.1.exe",
  "C:\\Installers\\ImageManager5.exe",
  "C:\\Installers\\ImageManagerSetup.exe",
  "C:\\Installers\\ShadowProtect_MSP_4.2.7.zip",
  "C:\\Installers\\ShadowProtectSetup_MSP_4.2.5.exe",
  "C:\\Installers\\VMware-Server-2.0.2-203138.exe",
  "C:\\Installers\\control center installer.exe",
  "C:\\Installers\\BDR Restore.iso",
  "C:\\Installers\\storagecraft.iso",
  "C:\\Installers\\ImageManager_Setup_6.7.7.exe",
  "C:\\Installers\\ImageManager-6-Setup.exe",
  "C:\\installers\\uAgent.exe",
  "C:\\installers\\ImageManager_Setup_7.0.5.exe"
]

# clean up the files
files_to_cleanup.each do |f|
  file f do
    action :delete
    ignore_failure true
  end
end


# Image Verification Files
remote_file 'C:\\BDR\\ImageVerification\\ImageVerification.exe' do
  source "#{ftp_url}/ImageVerification/ImageVerification.exe"
  action :create_if_missing
  checksum '70b8326637d6fcbb6863898939c5fbebc51563a59b7ad0bba7558c9d75daa6a3'
  ignore_failure true
end

remote_file 'C:\\BDR\\ImageVerification\\ImageVerification.sln' do
  source "#{ftp_url}/ImageVerification/ImageVerification.sln"
  action :create_if_missing
  checksum '0a3bec1703e7f873710cbe0c4393a0f379a6561c7f20f47878a65b825a4b1d0f'
  ignore_failure true
end

remote_file 'C:\\BDR\\ImageVerification\\ImageVerification.suo' do
  source "#{ftp_url}/ImageVerification/ImageVerification.suo"
  action :create_if_missing
  checksum '66f7685a0dd09f1ca6554472300e535c7c5c3dbda1fd298d78ab02840226a1a7'
  ignore_failure true
end

remote_file 'C:\\BDR\\ImageVerification\\Telerik.WinControls.dll' do
  source "#{ftp_url}/ImageVerification/Telerik.WinControls.dll"
  action :create_if_missing
  checksum '7af0cfc4e9269a6ec11e1dead6789ea72440687e81c8683d5bfbad6a70f8ea57'
  ignore_failure true
end

remote_file 'C:\\BDR\\ImageVerification\\Telerik.WinControls.GridView.dll' do
  source "#{ftp_url}/ImageVerification/Telerik.WinControls.GridView.dll"
  action :create_if_missing
  checksum '831c887057c01514df6a3e7ff4ee4b396e2e3df25bd64b813392bc3850edff92'
  ignore_failure true
end

remote_file 'C:\\BDR\\ImageVerification\\Telerik.WinControls.GridView.xml' do
  source "#{ftp_url}/ImageVerification/Telerik.WinControls.GridView.xml"
  action :create_if_missing
  checksum 'a27d2b20147076a08f53e9b1319271956f7dc72e472031a0b0198ae14da2bbd4'
  ignore_failure true
end

remote_file 'C:\\BDR\\ImageVerification\\Telerik.WinControls.UI.dll' do
  source "#{ftp_url}/ImageVerification/Telerik.WinControls.UI.dll"
  action :create_if_missing
  checksum 'f3f6ed9d4c4e87160114acadcb3a1cfaa9ecd7096d123c180cd490c7458c797a'
  ignore_failure true
end

remote_file 'C:\\BDR\\ImageVerification\\TelerikCommon.dll' do
  source "#{ftp_url}/ImageVerification/TelerikCommon.dll"
  action :create_if_missing
  checksum '2264aa4d0edcb64f5d1ed644ebc6ce3776653c2b9dc87b8e37d4e5a2509ff0ea'
  ignore_failure true
end
