# requires pswindowsupdate package from chocolatey (installed by other script)
Import-Module PSWindowsUpdate

Get-WUUninstall -KBArticleID KB3035583 -AcceptAll -IgnoreReboot #win10
Hide-WUUpdate -KBArticleID KB3035583 -AcceptAll -IgnoreReboot #win10
Hide-WUUpdate -KBArticleID KB2952664 -AcceptAll -IgnoreReboot #win8
