echo "Setting GoogleDNS for ARRC Systems..."
$GETPARTNERID = Get-Content c:\BDR\serial.txt -ErrorAction SilentlyContinue
IF($GETPARTNERID -eq "1001214") {
  netsh interface ip add dns "Local Area Connection" 8.8.8.8 index=2
} ELSE {
  echo "Not an ARRC System"
}
