
$tvPasswordValue = "53610FC9FE60476BD4BAFA387CE95A76";
$tvIdValue = "chartec";
$tvRegPath = "HKLM:\SOFTWARE\Wow6432Node\TeamViewer\Version8";


function main() {
  $regData = getTvRegData;

  $shouldRestartTv = $false;

  if (disableWindowsLogin($regData)) {
    Write-Output "Disabled TV Windows logon.";
    $shouldRestartTv = $true;
  } else {
    Write-Output "TV Windows logon already disabled.";
  }

  if (setTvPassword($regData)) {
    Write-Output "Added CharTec entry to TV auth.";
    $shouldRestartTv = $true;
  } else {
    Write-Output "CharTec entry already in TV auth.";
  }

  if ($shouldRestartTv) {
    restartTv;
  }
}


function disableWindowsLogin($regData) {
  if ($regData.Security_WinLogin) {
    Remove-ItemProperty -path $tvRegPath -name "Security_WinLogin";
    return $true;
  } else {
    return $false;
  }
}


function setTvPassword($regData) {
  $data = getTvPwdData($regData);

  $entries = $data.entries;
  $shouldRestart = !$data.hasCharTec;

  if (!$data.hasCharTec) {
    $entries += @{
      "id"       = $tvIdValue;
      "password" = $tvPasswordValue;
    };
  }
  
  # lists (of strings!) because the reg values are of type MultiString
  $ids = @();
  $passwords = @();
  
  foreach ($entry in $entries) {
    $id = [string]$entry.id;
    $pass = [string]$entry.password;

    # filter out old chartec passwords
    $isCharTecId = $id -eq $tvIdValue;
    $isNotCharTecPassword = $pass -ne $tvPasswordValue;
    if ($isCharTecId -and $isNotCharTecPassword) {
      $shouldRestart = $true;
      continue;
    } else {
      $ids += $id;
      $passwords += $pass;
    }
  }

  if (!$regData.MultiPwdMgmtIds -or !$regData.MultiPwdMgmtPWDs) {
    New-ItemProperty -path $tvRegPath -PropertyType MultiString -name MultiPwdMgmtIds -value $ids;
    New-ItemProperty -path $tvRegPath -PropertyType MultiString -name MultiPwdMgmtPWDs -value $passwords;
  } else {
    Set-ItemProperty -path $tvRegPath -name MultiPwdMgmtIds -value $ids;
    Set-ItemProperty -path $tvRegPath -name MultiPwdMgmtPWDs -value $passwords;
  }
  
  return $shouldRestart;
}


function getTvPwdData($regData) {
  $idStr = [string]$regData.MultiPwdMgmtIds;
  $passwordStr = [string]$regData.MultiPwdMgmtPWDs;

  if (!$idStr -or !$passwordStr) {
    return @{
      "entries" = @();
      "hasCharTec" = $false;
    };
  } else {
    $list = @();
    $ids = $idStr.Split();
    $passwords = $passwordStr.Split();

    $hasCharTec = $false;
    $n = 0;
    foreach ($id in $ids) {
      $password = $passwords[$n];

      $isCharTecPassword = $password -eq $tvPasswordValue;
      $isCharTecId = $id -eq $tvIdValue;

      if ($isCharTecPassword -and $isCharTecId) { $hasCharTec = $true; }

      $list += @{
        "id"       = $id;
        "password" = $password;
      };
      $n += 1;
    }

    return @{
      "entries"    = $list;
      "hasCharTec" = $hasCharTec;
    };
  }
}


function getTvRegData() {
  return Get-ItemProperty -path $tvRegPath;
}


function restartTv() {
  Write-Output "Restarting TeamViewer";
  Restart-Service TeamViewer8;
}


main;



