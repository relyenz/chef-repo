# removed member check
# $memberid = Get-Content c:\BDR\serial.txt -ea SilentlyContinue;

# $acceptedMembers = "10019055", "10018791", "1007864", "1001214", "10012632", "1008245"; # an array of strings containing valid member ids
$shareNames = 'Backup', 'F', 'Installers';

function enactShareProtection($share) {
  Grant-SmbShareAccess -Name $share -AccountName 'Everyone' -AccessRight Read -Force | Out-Null;
  Grant-SmbShareAccess -Name $share -AccountName 'arrcnetadmin' -AccessRight Full -Force | Out-Null;
  Grant-SmbShareAccess -Name $share -AccountName 'bdrtask' -AccessRight Full -Force | Out-Null;
  Grant-SmbShareAccess -Name $share -AccountName 'BUILTIN\Administrators' -AccessRight Full -Force | Out-Null;
}

function checkIfShareExists($share) {
  $test = Get-SmbShare -Name $share -ea SilentlyContinue;
  if ($test) {
    return $true;
  } else {
    return $false;
  }
}

# removed member check
# if ($acceptedMembers -contains $memberid) {
#   foreach ($shareName in $shareNames) {
#     $check = checkIfShareExists $shareName;
#     if ($check) {
#       enactShareProtection $shareName;
#       Get-SmbShareAccess -Name $shareName;
#     }
#   }
# }

foreach ($shareName in $shareNames) {
    $check = checkIfShareExists $shareName;
    if ($check) {
      enactShareProtection $shareName;
      Get-SmbShareAccess -Name $shareName;
    }
    # check for duplicates
    $shareName2 = $shareName + "2";
    $check2 = checkIfShareExists $shareName2;
    if ($check2) {
      enactShareProtection $shareName2;
      Get-SmbShareAccess -Name $shareName2;
    }
}


<# 
 ### For setting audit ACLs

$path = "F:\Backup";

$AuditUser = "Everyone";
$AuditRules = "Delete,DeleteSubdirectoriesAndFiles";
$InheritType = "ContainerInherit,ObjectInherit";
$AuditType = "Success";
$AuditRule = New-Object System.Security.AccessControl.FileSystemAuditRule(
    $AuditUser, $AuditRules, $InheritType, "None", $AuditType
);

$acl = Get-Acl $path;
$acl.SetAuditRule($AuditRule);
$acl | Set-Acl $path;


#>