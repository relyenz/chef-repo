
$newest = @{};
$newest.major = 1;
$newest.minor = 01;

$zipfile = "C:\temp\pyupdate_1.01.zip";

function main() {
  Write-Output "Getting pyUpdate version...";

  $version = getVersion;

  Write-Output $version;

  $noVersion = $version.full -eq 0;

  if ($noVersion) {
    Write-Output "pyUpdate not found!";
  }

  $oldMajor = $version.major -lt $newest.major;
  $oldMinor = !$oldMajor -and ($version.minor -lt $newest.minor);
  $oldVersion = $oldMajor -or $oldMinor;

  $shouldUpdate = $oldVersion;

  Write-Output ("Should update pyUpdate? " + $shouldUpdate);

  if ($shouldUpdate) {
    update;
  }
}

function update() {
  $check = Test-Path($zipfile);
	if (!$check) {
		Write-Output("ERROR! Missing new pyUpdate zipfile at: $zipfile");
	} else {
		Write-Output("Updating pyUpdate...");
    Expand-ZIPFile -File $zipfile -Destination "C:\temp\pyupdate";
    Copy-Item -path "C:\temp\pyupdate\pyupdate.exe" -destination "C:\Program Files\CharTec" -Force;
	}
}

function getVersion() {
  $result = @{};
  $result.full = 0;

  $test = &"C:\Program Files\CharTec\pyupdate.exe" --version;
  $check = $test -match '\d+[.]\d+';
  if ($check) {
    $arr = $test.split(".");
    $result.full = $test;
		$result.major = $arr[0];
		$result.minor = $arr[1];
  }

  return $result;
}

# https://www.howtogeek.com/tips/how-to-extract-zip-files-using-powershell/
function Expand-ZIPFile ($file, $destination) {
  $shell = new-object -com shell.application;
  $zip = $shell.NameSpace($file);
  foreach($item in $zip.items()) {
    $shell.Namespace($destination).copyhere($item);
  }
}

main;
