#
# Cookbook Name:: bdr_fixes
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# Resources used
# https://docs.chef.io/resource_registry_key.html

require 'chef/win32/version'
windows_version = Chef::ReservedNames::Win32::Version.new

# array of strings representing services to be started
services_to_start = [
  "SAVService", # Sophos
  "chef-client",
  "CharTecBDRCollector",
  "CharTec Agent",
  "ScreenConnect Client (54d9f2ec98463e54)",
  "TeamViewer8",
  "Zabbix Agent"
]

services_to_start.each do |s|
  service s do
    action :start
    ignore_failure true
  end
end


# arrcnetadmin Administrators group
group "Administrators" do
  action :modify
  members "arrcnetadmin"
  append true
  ignore_failure true
end

# file-sharing performance fixes
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\Session Manager\\Memory Management" do
  values [{
    :name => 'LargeSystemCache',
    :type => :dword,
    :data => 1
    }]
end
registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\LanmanServer\\Parameters" do
  values [{
    :name => 'Size',
    :type => :dword,
    :data => 3
    }]
end
batch 'autodisconnect' do
  code "net config server /autodisconnect:03"
  ignore_failure true
end

windows_auto_run 'DoyenzTrayMonitor' do
  action :remove
end

# Mike B's power config script
batch 'set power config' do
  code <<-EOH
    @echo off
    Powercfg -getactivescheme
    Powercfg -change -disk-timeout-ac 0
    Powercfg -change -disk-timeout-dc 0
    Powercfg -change -standby-timeout-ac 0
    Powercfg -change -standby-timeout-dc 0
    Powercfg -change -hibernate-timeout-ac 0
    Powercfg -change -hibernate-timeout-dc 0
    EOH
  ignore_failure true #seems to be possible for this to fail based on group policy restrictions
end

# remove old backup scripts that may contain sensitive data
powershell_script 'remove backup scripts' do
  code <<-EOH
    Remove-Item C:\\BDR\\scripts\\*.vbs -Recurse -Force -ErrorAction SilentlyContinue
    EOH
  ignore_failure true
end

# backup share protection script, enforces permissions on select fileshares
cookbook_file "C:\\temp\\backupShareProtection.ps1" do
  source "backupShareProtection.ps1"
end
execute 'backup share protection' do
  command 'powershell -ExecutionPolicy Bypass -file C:\\temp\\backupShareProtection.ps1'
end

# automatically add Google public DNS to ARRC BDRs
cookbook_file "C:\\temp\\aDNS.ps1" do
  source "arrcDNS.ps1"
end
execute 'ARRC DNS' do
  command 'powershell -ExecutionPolicy Bypass -file C:\\temp\\aDNS.ps1'
end


# hide particular updates on Windows 7 (prevent upgrade to Win 10)
if windows_version.windows_7?
  cookbook_file "C:\\temp\\stopWindows7to10.ps1" do
    source "stopWindows7to10.ps1"
  end
  execute 'Stop Windows 7 to 10 Update' do
    command 'powershell -ExecutionPolicy Bypass -file C:\\temp\\stopWindows7to10.ps1'
  end
end

# stop and disable VboxWeb service (not used and causes other issues)
service 'VirtualBoxWeb' do
  action [ :stop, :disable ]
  only_if {::Win32::Service.exists?("VirtualBoxWeb")}
  ignore_failure true
end

# stop and disable Apache2.4 (not in use and not updated for security)
service 'Apache2.4' do
  action [ :stop, :disable ]
  only_if {::Win32::Service.exists?("Apache2.4")}
  ignore_failure true
end

# fix for non-updatable pyupdate version
remote_file 'C:\\temp\\pyupdate_1.01.zip' do
  source "https://centralpoint.relyenz.com/downloads/agent/pyupdate_1.01.zip"
  action :create
  ignore_failure true
end
cookbook_file "C:\\temp\\pyupdateUpdate.ps1" do
  source "pyupdateUpdate.ps1"
end
execute 'Update pyUpdate' do
  command 'powershell -ExecutionPolicy Bypass -file C:\\temp\pyupdateUpdate.ps1'
end

cookbook_file "C:\\temp\\teamviewerAuth.ps1" do
  source "teamviewerAuth.ps1"
end
execute 'Update TeamViewer Auth' do
  command 'powershell -ExecutionPolicy Bypass -file C:\\temp\teamviewerAuth.ps1'
end

execute 'Allow Writes to CharTec Folder' do
  command 'attrib -r "C:\Program Files\CharTec"'
  ignore_failure true
end
