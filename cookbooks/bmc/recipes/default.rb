#
# Cookbook Name:: bmc
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

require 'chef/win32/version'
windows_version = Chef::ReservedNames::Win32::Version.new

# make sure the C:\BDR folder exists
directory 'C:\\BDR'

# install dotnet35
include_recipe 'ms_dotnet'
ms_dotnet_framework '3.5' do
  action :install
  ignore_failure true
end


# ftp_url = 'ftp://chartecftp:Wow%405449@ftp.chartec.net/Roger'
cp_url = "https://centralpoint.chartec.net/downloads/chef_data"

# install BMC
windows_package 'BDR Management Tool' do
  installer_type :msi
  options '/quiet /qn /norestart'
  source "#{cp_url}/bdr_software/BDRInstaller.msi"
  not_if { ::File.exists?('C:\\BDR\\CharTec BDR.exe') }
end
