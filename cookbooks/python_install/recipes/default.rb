#
# Cookbook Name:: python_install
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

#############################################
# Whole script has been commented because some runlists are still trying to run it.
#
#############################################

# include_recipe 'chocolatey'
# ::Chef::Recipe.send(:include, Windows::Helper) # needed for choco.. I think?

# # Python 3.5 detection
# is_python_351_installed = is_package_installed?('Python 3.5.1 (32-bit)')
# is_python_351_present = ::File.exists?('C:\\Program Files (x86)\\Python35-32\\python.exe')

# # Python 3.4 detection
# is_python_344_pkg_installed = is_package_installed?('Python 3.4.4')
# is_python_344_present = ::File.exists?('C:\\Python34\\python.exe') #sometimes 3.4 doesn't show up in add/remove programs...
# is_python_344_installed = is_python_344_pkg_installed || is_python_344_present # if either one

# # vcredist2015 detection
# is_vc_redist_2015_app_installed = is_package_installed?('Microsoft Visual C++ 2015 Redistributable (x86) - 14.0.23918') # this is what shows up in appwiz.cpl
# is_vc_redist_2015_min_installed = is_package_installed?('Microsoft Visual C++ 2015 x86 Minimum Runtime - 14.0.23506') # this is what shows up in registry
# is_vc_redist_2015_installed = is_vc_redist_2015_app_installed || is_vc_redist_2015_min_installed # if either one is true

# chocolatey 'chocolatey' # makes sure chocolatey is installed so we can install packages

# # attempt to install vcredist2015, required for Python 3.5
# chocolatey_package 'vcredist2015' do
#   ignore_failure true
#   not_if {is_vc_redist_2015_installed}
# end

# # Windows update fix for python, attempts to install available updates and then specific updates
# # stopped running the all updates line because it takes forever to run and we may not want to install ALL available updates...
# chocolatey_package 'pswindowsupdate' do
#   ignore_failure true
# end

# powershell_script 'install-required-updates' do
#   code <<-EOH
#     Import-Module PSWindowsUpdate
#     # Get-WUInstall -AcceptAll -IgnoreReboot
#     Get-WUInstall -KBArticleID 2887595 -AcceptAll -IgnoreReboot
#     Get-WUInstall -KBArticleID 2919442 -AcceptAll -IgnoreReboot
#     Get-WUInstall -KBArticleID 2919355 -AcceptAll -IgnoreReboot
#     Get-WUInstall -KBArticleID 2999226 -AcceptAll -IgnoreReboot
#     # $test = Get-WURebootStatus -Silent
#     EOH
#   not_if {is_vc_redist_2015_installed && is_python_351_installed}
#   ignore_failure true
# end

# # install python 3.4.4 if no python is installed
# windows_package 'Python 3.4.4' do
#   source 'https://www.python.org/ftp/python/3.4.4/python-3.4.4.msi'
#   installer_type :msi
#   options '/qn'
#   not_if {is_python_351_installed || is_python_344_installed}
# end

# # attempt to install python 3.5.1, may fail if required Windows updates or vcredist2015 are not available
# windows_package 'Python 3.5.1 (32-bit)' do
#   source 'https://www.python.org/ftp/python/3.5.1/python-3.5.1.exe'
#   options '/quiet InstallAllUsers=1 PrependPath=1'
#   installer_type :custom
#   action :install
#   timeout 600 # 10 minutes
#   retries 1
#   only_if {is_vc_redist_2015_installed}
#   not_if {is_python_351_installed}
#   ignore_failure true
# end

# # add python 3.5 to windows path if it's installed
# windows_path 'C:\\Program Files (x86)\\Python35-32\\' do
#   action :add
#   only_if {is_python_351_installed}
# end

# # add python 3.4 to path if it is installed and 3.5 is not
# windows_path 'C:\\Python34\\' do
#   action :add
#   only_if {is_python_344_installed && !is_python_351_installed}
# end

# # remove 3.4 from path if 3.5 is installed
# windows_path 'C:\\Python34\\' do
#   action :remove
#   only_if {is_python_351_installed}
# end

# # remove python 3.3 from path
# windows_path 'C:\\Python33\\' do
#   action :remove
# end
