#
# Cookbook Name:: bdr_check
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# things to do at the end of the chef run

# below shouln't be neccessary anymore since we're installing python 3.4.4 as a backup and that seems to work consistently
#
# is_python_351_installed = is_package_installed?('Python 3.5.1 (32-bit)')
# is_python_344_installed = is_package_installed?('Python 3.4.4')
# batch 'python not installed msg' do
#   code <<-EOH
#     @echo off
#     msg * "Python may not be installed on this system yet. Please check that Python 3.5.1 (32-bit) is installed, or run Windows updates and reboot."
#     EOH
#   not_if {is_python_351_installed || is_python_344_installed}
# end

# write git short rev to file
cookbook_file "C:\\temp\\gitCheck.ps1" do
  source "gitCheck.ps1"
end
execute 'Git Rev Check' do
  command 'powershell -ExecutionPolicy Bypass -file C:\\temp\\gitCheck.ps1'
  ignore_failure true
end

# write timestamp to a file for checking by other recipes/scripts/software
unixtime = Time.now.to_i
file 'C:\\chef\\lastrun.txt' do
  content "#{unixtime}"
end

# execute 'pyversion' do
#   ignore_failure true
#   command 'python -V > C:\\temp\\pyver.txt'
# end
