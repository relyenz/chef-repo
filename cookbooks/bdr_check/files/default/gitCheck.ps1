
$test = Test-Path C:\chef\repo\.git;

if (!$test) {
  exit;
}

Set-Location C:\chef\repo;

$rev = &"git" rev-parse --short HEAD;

$clean_rev = $rev.Trim();

if ($clean_rev.length -gt 8) {
  Write-Host "Rev cmd output too long.";
  exit;
}

$filepath = "C:\chef\repo_rev.txt";

# this one put a newline after the text
# Out-File -inputObject $clean_rev -filePath $filepath -encoding utf8;

# this one does not
[System.IO.File]::WriteAllText($filepath, $clean_rev, [System.Text.Encoding]::ASCII);
