#
# Cookbook Name:: bdr_installs
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# Resources used
# https://docs.chef.io/resource_windows_package.html
# https://github.com/chef-cookbooks/windows

# install/update packages from Chocolatey
# should run first, installs things needed by other scripts
cookbook_file "C:\\temp\\chocolateyBDR.ps1" do
  source "chocolateyBDR.ps1"
end
execute 'Chocolatey BDR Packages' do
  command 'powershell -ExecutionPolicy Bypass -file C:\\temp\\chocolateyBDR.ps1'
  ignore_failure true
end

# ensure git is on the PATH
windows_path 'C:\Program Files\Git\cmd' do
  action :add
end

# hacks to try to make sure git is on the PATH
chefgit_env = "#{ENV['Path']};C:\\Program Files\\Git\\cmd";
chefgit_hash = {'Path' => chefgit_env}
ENV['Path'] = chefgit_env

# update local chef repo, requires git installed by chocolatey
git 'C:\\chef\\repo' do
  repository 'https://bitbucket.org/relyenz/chef-repo.git'
  revision 'master'
  action :sync
  environment chefgit_hash
  ignore_failure true
end

cookbook_file "C:\\temp\\localRepoMigration.ps1" do
  source "localRepoMigration.ps1"
end
execute 'Chef Local Repo Migration' do
  command 'powershell -ExecutionPolicy Bypass -file C:\\temp\\localRepoMigration.ps1'
  ignore_failure true
end


windows_feature 'TelnetClient' do
  action :install
  ignore_failure true
end

# TODO: merge the following two scripts
# Windows updates script
cookbook_file "C:\\temp\\winUpdates.ps1" do
  source "winUpdates.ps1"
end
execute 'Required Windows Updates' do
  command 'powershell -ExecutionPolicy Bypass -file C:\\temp\\winUpdates.ps1'
  ignore_failure true
end

# Windows security updates script
cookbook_file "C:\\temp\\securityUpdates.ps1" do
  source "securityUpdates.ps1"
end
execute 'Windows Security Updates' do
  command 'powershell -ExecutionPolicy Bypass -file C:\\temp\\securityUpdates.ps1'
  ignore_failure true
end

ftp_url = 'ftp://chartecftp:Wow%405449@ftp.chartec.net/Roger'
cp_url = "https://centralpoint.chartec.net/downloads/chef_data"

cookbook_file "C:\\temp\\sophosUpdate.ps1" do
  source "sophosUpdate.ps1"
end
execute 'Sophos Update' do
  command 'powershell -ExecutionPolicy Bypass -file C:\\temp\\sophosUpdate.ps1'
  ignore_failure true
end


# write the current ShadowProtect version to a temp file
# this is due to Chef not detecting the version properly, so we'll do it ourselves and use it below
powershell_script 'SPversion' do
  code <<-EOH
    $SPversion = (Get-Command "C:\\Program Files (x86)\\StorageCraft\\ShadowProtect\\ShadowProtect.exe" -ErrorAction SilentlyContinue).FileVersionInfo.ProductVersion
    # Out-File -FilePath 'C:\\temp\\spversion.txt' -InputObject $SPversion #writes out a newline after version
    [io.file]::WriteAllText("C:\\temp\\spversion.txt", $SPversion) #doesn't write out a newline after version
  EOH
  ignore_failure true
end

sp_installer = 'C:\\installers\ShadowProtectSetup_MSP_5.2.7.exe'

# disabled due to issues with SP5 and SPX on the same machine
# windows_package 'StorageCraft ShadowProtect' do
#   installer_type :custom
#   options 'Install IACCEPT=STORAGECRAFT.EULA LANG=en FORCE=Yes'
#   source sp_installer
#   not_if {::File.read('C:\\temp\\spversion.txt') == '5.2.7.38915'}
#   only_if {::File.exists?(sp_installer)} #make sure we downloaded the installer already
# end

# attempt to update ImageManager
cookbook_file "C:\\temp\\imUpdate.ps1" do
  source "imUpdate.ps1"
end
execute 'ImageManager Update' do
  command 'powershell -ExecutionPolicy Bypass -file C:\\temp\\imUpdate.ps1'
end

# windows_package 'Doyenz Agent for ShadowProtect™' do
#   installer_type :custom
#   options '/S /RUNSERVICE=YES /RUNTRAYMONITOR=no'
#   source "#{ftp_url}/DoyenzAgentInstaller.exe"
#   not_if {::Win32::Service.exists?("DoyenzShadowCloud1")} # need to check if the service is already there because Accelerite uses a dumb '™' even in the registry
#   # leading '::' tells Chef to look outside its namespace
#   # http://stackoverflow.com/questions/20131754/chef-how-do-i-check-to-see-if-a-service-is-installed
#   ignore_failure true
# end

windows_package 'ScreenConnect Client (54d9f2ec98463e54)' do
  installer_type :msi
  options '/quiet /qn /norestart'
  source "#{cp_url}/remote_access/ScreenConnect.ClientSetup-ClassicBDR.msi"
  not_if {::Win32::Service.exists?("ScreenConnect Client (54d9f2ec98463e54)")}
end

windows_package 'TeamViewer 8 Host' do
  installer_type :custom
  source "#{cp_url}/remote_access/CT-TeamViewer.exe"
  not_if {::Win32::Service.exists?("TeamViewer8")}
end


windows_zipfile 'C:\\BDR' do
  source "#{cp_url}/bdr_software/Collector.zip" # zip contains a folder named 'Collector', therefore unzip to root of C:\BDR
  action :unzip
  not_if {::File.exists?("C:\\BDR\\Collector\\CharTecBDRCollector.exe")}
end
