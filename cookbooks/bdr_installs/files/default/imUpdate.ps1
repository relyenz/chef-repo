

$newest = @{};
$newest.major = 7;
$newest.minor = 1;

$installer = "C:\Installers\ImageManager_Setup_7.1.0.exe";


function main() {
	Write-Output("Getting IM version...");

	$version = getVersion;

	Write-Output($version);

	$checks = @{};

	$checks.noVersion = $version.full -eq 0;
	$checks.badVersion = $version.major -lt 7;
	$checks.latestVersion = $version.major -ge $newest.major -and $version.minor -ge $newest.minor;

	$shouldUpdate = (!$checks.noVersion -and !$checks.badVersion -and !$checks.latestVersion);

	Write-Output($checks);
	Write-Output("Should update ImageManager? "+$shouldUpdate);

	if ($shouldUpdate) {
		updateIM;
	}
}


function updateIM() {
	$check = Test-Path($installer);
	if (!$check) {
		Write-Output("ERROR! Missing new ImageManager installer at: $installer");
	} else {
		$cmd = "&'$installer' /quiet IACCEPT=STORAGECRAFT.EULA Reboot=No";
		Write-Output("Updating ImageManager...");
		Invoke-Expression($cmd);
		Start-Service -name "StorageCraft ImageManager";
	}
}


function getVersion() {
	$test = Invoke-Expression("&'C:\Program Files\CharTec\pyagent\pyagent.exe' --im version");
	$obj = @{};
	$check = $test -match '\d+[.]\d+[.]\d+[.]\d+';
	if (!$check) {
		$obj.full = 0;
		return $obj;
	} else {
		$arr = $test.split(".");
		$obj.full = $test;
		$obj.major = $arr[0];
		$obj.minor = $arr[1];
		$obj.patch = $arr[2];
		$obj.build = $arr[3];
		return $obj;
	}
}

# run the main func from top of file, allows other funcs to be read first
main;