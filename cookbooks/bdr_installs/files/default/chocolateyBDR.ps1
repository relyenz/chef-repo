# check if chocolatey is installed
$test = &"choco";

# install chocolatey if missing, else update
if (!$test) {
  Write-Output "Chocolatey missing from PATH, installing...";
  Set-ExecutionPolicy Bypass -Force;
  Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'));
} else {
  Write-Output "Chocolatey detected in PATH, checking for updates...";
  &"cup" chocolatey -y;
}

$packages = "dotnet3.5",
"dotnet4.5",
"powershell",
"vcredist2012",
"googlechrome",
"notepadplusplus.install",
'7zip.install',
'javaruntime',
'filezilla',
'winscp',
'putty',
'nano',
'curl',
'wget',
'windirstat',
'procmon',
'baretail',
'autoruns',
'hashtab',
"pswindowsupdate",
"git";

# this doesn't work, but passing in the whole list does.. whatever
# $packageList = $packages -Join " ";

Write-Output "Installing/updating Chocolatey packages...";
&"cup" $packages -y;


# below are hacks to try to ensure git is installed and on the PATH

# if we didn't JUST install chocolatey
if ($test) {
  Write-Output "Checking for Git in PATH...";
  $testGit = &"git";

  if (!$testGit) {
    Write-Output "Git test failed, forcing reinstall...";
    &"cinst" git --force;
  }
}

# Write-Output "Checking required PATH variables...";

# $currentPath = (Get-ItemProperty -Path ‘Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment’ -Name PATH).Path;

# $vars = $currentPath.split(";");

# $expectedGitPath = "C:\Program Files\Git\cmd";

# $hasGit = $false;

# foreach($var in $vars) {
#   if ($var -like $expectedGitPath) {$hasGit = $true}
# }

# if ($hasGit -eq $false) {
#   Write-Output "Git not found in path, adding...";
#   $newPath = "$currentPath;$expectedGitPath";
#   Set-ItemProperty -Path ‘Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment’ -Name PATH –Value $newPath;
# }


