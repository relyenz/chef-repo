# requires pswindowsupdate package from chocolatey (installed by other script)
Import-Module PSWindowsUpdate;

$kbList = @("KB2999226"); # Win7 fix for pyInstaller DLL issues

Get-WUInstall -KBArticleID $kbList -AcceptAll -IgnoreReboot;

# WINDOWS 10 HANDLING
$tenUpdatesDeferRegPath = "HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings";
$winVer = [System.Environment]::OSVersion.Version;

$isTen = $winVer.Major -eq 10;
$writeIsCreators = 0;

if ($isTen) {
  $isCreators = $isTen -and ($winVer.Build -ge 15063);
  if ($isCreators) {
    $writeIsCreators = 1;
    Enable-WindowsOptionalFeature -Online -FeatureName:Microsoft-Hyper-V -All -NoRestart
  }

  $isNewDeferType = $winVer.Build -ge 15002;
  if ($isNewDeferType) {
    Set-ItemProperty -path $tenUpdatesDeferRegPath -name BranchReadinessLevel -value 20;
    Set-ItemProperty -path $tenUpdatesDeferRegPath -name DeferFeatureUpdatesPeriodInDays -value 90;
  } else {
    Set-ItemProperty -path $tenUpdatesDeferRegPath -name DeferUpgrade -value 1;
  }
}

Out-File -inputObject $writeIsCreators -filePath C:\BDR\is_win10_creators.txt -encoding utf8;