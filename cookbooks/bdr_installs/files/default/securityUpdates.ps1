# requires pswindowsupdate package from chocolatey (installed by other script)
Import-Module PSWindowsUpdate;

# created for WannaCry exploit patches, modifyable for others

# WannaCry: https://technet.microsoft.com/en-us/library/security/ms17-010.aspx, https://support.microsoft.com/en-us/help/4013389/title
$kbList = "KB4012598", # Server 2008
	"KB4012212", # Win 7 / Server 2008 R2
	"KB4012214", # Server 2012
	"KB4012213", # Win 8.1 / Server 2012 R2
	"KB4012606", # Win 10
	"KB4013198", # Win 10 b1511
	"KB4013429", # Win 10 b1607
	# Meltdown: https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/ADV180002
	"KB4056898", # Windows 8.1 / Server 2012 R2
	"KB4056897", # Server 2008 R2 / Windows 7
	"KB4056894", # Server 2008 R2 / Windows 7 (rollup)
	"KB4056890", # Server 2016 / Windows 10 v1607
	"KB4056899", # Server 2012 -- not listed in advisory: https://support.microsoft.com/en-us/help/4056899/windows-sever-2012-update-kb4056899
	"KB4056896", # Server 2012 (rollup) -- not listed in advisory: https://support.microsoft.com/en-us/help/4056896/windows-sever-2012-update-kb4056896
	"KB4056892" # Windows 10 v1709

Get-WUInstall -KBArticleID $kbList -AcceptAll -IgnoreReboot;