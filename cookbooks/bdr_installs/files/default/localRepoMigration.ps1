
# can use generic config for local repo, doesn't even need node name
$config = @"
# chartec_chef_2
# if this file stops reading properly, make sure it is still encoded utf8:
# http://stackoverflow.com/questions/36464364/chef-client-using-default-config-values-despite-presence-of-client-rb-no-error
#
log_level :info
log_location STDOUT
interval 21600 # 6 hours
chef_repo_path 'C:\chef\repo'
local_mode true
json_attribs 'C:\chef\repo\bdr_runlist.json'
"@;

function main() {
  $repoGood = (Test-Path C:\chef\repo\.git) -and (Test-Path C:\chef\repo\bdr_runlist.json);

  if (!$repoGood) {
    Write-Host "Chef repo or runlist not detected, cannot make config changes.";
    exit;
  }

  if (checkConfig) {
    Write-Host "Chef config already up-to-date.";
    exit;
  }

  # DEFINITELY ALWAYS OUTPUT UTF8 FOR CHEF:
  # http://stackoverflow.com/questions/36464364/chef-client-using-default-config-values-despite-presence-of-client-rb-no-error
  #
  Out-File -inputObject $config -filePath C:\chef\client.rb -encoding utf8;

  Write-Host "Successfully updated Chef config to use local repo!";
}


function checkConfig() {
  $file = Get-Content "C:\chef\client.rb";
  $containsId = $file | %{$_ -match "chartec_chef_2"}
  return $containsId -contains $true;
}

main;