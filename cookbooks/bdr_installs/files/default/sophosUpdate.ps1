
$endpointInstaller = "C:\Temp\SophosInstall_Server.exe";
$serverInstaller = "C:\Temp\SophosInstall_Endpt.exe";
$installerArgs = "--quiet";

$installPath = "C:\Program Files (x86)\Sophos\Management Communications System\Endpoint\McsClient.exe";

function main() {
  $installed = Test-Path($installPath);

  if (!$installed) {
    installSophos;
  } else {
    Write-Output("Sophos already installed!");
  }
}


function installSophos() {
  $installer = "";
  if (isWorkstation) {
    Write-Output("Using workstation installer...");
    $installer = $endpointInstaller;
  } else {
    Write-Output("Using server installer...");
    $installer = $serverInstaller;
  }

  $check = Test-Path($installer);
  if (!$check) {
    Write-Output("ERROR! Missing new Sophos installer at: $installer");
  } else {
    Write-Output("Updating Sophos...");
    Start-Process $installer -ArgumentList $installerArgs -Wait;
  }
}

function isWorkstation() {
  $productTypeReg = Get-ItemProperty("HKLM:\SYSTEM\CurrentControlSet\Control\ProductOptions");
  $productType = $productTypeReg.ProductType;
  return $productType -eq "WinNT";
}


main;
