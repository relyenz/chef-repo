
$newest = @{};
$newest.major = 9;
$newest.minor = 6;

$installer = "C:\Installers\Vipre_9-6_BDR.msi";

$oldInstallPath = "C:\Program Files (x86)\GFI Software\GFIAgent\";
$newInstallPath = "C:\Program Files (x86)\VIPRE Business Agent\";
$serviceExe = "SBAMSvc.exe";

$oldServicePath = $oldInstallPath + $serviceExe;
$newServicePath = $newInstallPath + $serviceExe;

# info for disabling Windows Defender
$defenderRegPath = "HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender";
$defenderRegProperty = "DisableAntiSpyware";
$defenderRegValue = "1";
$defenderRegType = "DWORD";

function main() {
  Write-Output "Getting Vipre version...";

  $version = getVersion;

  Write-Output $version;

  $noVersion = $version.full -eq 0;

  $oldMajor = $version.major -lt $newest.major;
  $oldMinor = !$oldMajor -and ($version.minor -lt $newest.minor);
  $oldVersion = $oldMajor -or $oldMinor;

  $sophos = checkSophos;

  $shouldUpdate = ($noVersion -or $oldVersion) -and !$sophos;

  Write-Output ("Should update Vipre? " + $shouldUpdate);

  if ($shouldUpdate) {
    updateVipre;
  } else {
    $defenderTest = Test-Path $defenderRegPath;

    Write-Output ("Should disable Windows Defender? " + $defenderTest);

    if ($defenderTest) {
      New-ItemProperty -Path $defenderRegPath -Name $defenderRegProperty -Value $defenderRegValue -PropertyType $defenderRegType;
    }
  }
}

function updateVipre() {
  $check = Test-Path($installer);
	if (!$check) {
		Write-Output("ERROR! Missing new Vipre installer at: $installer");
	} else {
		Write-Output("Updating Vipre...");
    Start-Process $installer -ArgumentList "/quiet" -Wait
	}
}

function getVersion() {
  $result = @{};
  $result.full = 0;

  if (Test-Path ($newServicePath)) {
    $fileToVersionCheck = $newServicePath;
    $checkRegex = '\d+[.]\d+[.]\d+[.]\d+';
  } elseif (Test-Path ($oldServicePath)) {
    $fileToVersionCheck = $oldServicePath;
    $checkRegex = '\d+[.]\d+[.]\d+';
  } else {
    return $result;
  }

  $test = (Get-Item $fileToVersionCheck).VersionInfo.ProductVersion;
  $check = $test -match $checkRegex;
  if ($check) {
    $arr = $test.split(".");
    $result.full = $test;
		$result.major = $arr[0];
		$result.minor = $arr[1];
		$result.patch = $arr[2];
  }

  return $result;
}

function checkSophos() {
  $test =  Test-Path C:\BDR\av_sophos_override.txt;
  if ($test) {
    Write-Output "Sophos override found.";
  }
  return $test;
}


main;
