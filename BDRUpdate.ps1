# PSUpdate "09-16-2015"

# ================================================================================================
#
#                                      New BDR Update Script
#
# ================================================================================================
    # Variables
            # Update Log Array
                $updatelogs = @()
            "Arguments: $($args.count)"
            $GETPARTNERID = Get-Content c:\BDR\serial.txt -ErrorAction SilentlyContinue
            $GETSITE = Get-Content c:\BDR\site.txt -ErrorAction SilentlyContinue

    # =============================================================================================
    # Update Log (Writes Successes and failures to Log)  / Updated by RL 2/26/14
    # =============================================================================================

        # Create TimeStamp

        # Create Update Log Directory
            if(!(Test-Path C:\BDR\Logs\BDRInfo\Update)) { New-Item C:\BDR\Logs\BDRInfo\Update -Type Directory }
        # Create Log File
            Set-Content -Path "C:\BDR\Logs\BDRInfo\Update\UpdateScript-$timestamp3.txt" -Value ""
    # =============================================================================================
    # Updated Wget to be a service  / Updated by ZJ 4/29/14
    # =============================================================================================

    # Functions

    # Functions
        function downloadfile ($filedownload, $filedestination)
            {
                $user      = "chartecftp"
                $pass      = "Wow@5449"
                $ftpserver = "ftp://ftp.chartec.net/Roger/"
                $filename  = $ftpserver+=$filedownload
                $arg       = "-O $filedestination $filename --user $user --password $pass"


                 IF(Test-Path C:\Installers\wget.exe)
                 {
                    $download = (Start-Process -FilePath "C:\Installers\wget.exe" -ArgumentList $arg -NoNewWindow -Wait).ExitCode

                         return $download
                 }
                    else
                        {
                            $webclient = New-Object System.Net.WebClient
                            $webclient.Credentials = New-Object System.Net.NetworkCredential($user,$pass)

                            $uri = New-Object System.Uri($filename)

                            $webclient.DownloadFile($uri, $filedestination)
                             Get-Job | Wait-Job
                        }
                }

    function createdirectories

        {

            IF(!(TEST-PATH C:\BDR\ImageVerification))
                {
                    NEW-ITEM c:\BDR\ImageVerification -type Directory
                    $updatelogs += "Created Directory ImageVerification [OK]"
                 }

            IF(!(TEST-PATH C:\BDR\HSR)) { NEW-ITEM C:\BDR\HSR -type Directory }

			IF(!(TEST-PATH C:\BDR\Logs\hsr)) { NEW-ITEM C:\BDR\Logs\hsr -type Directory }

            IF(!(TEST-PATH C:\Documentation))  { NEW-ITEM C:\Documentation -type Directory }

            IF(!(TEST-PATH C:\BDR\Production))  { NEW-ITEM C:\BDR\Production -type Directory }

            IF(!(Test-Path C:\BDR\Logs)) { NEW-ITEM C:\BDR\Logs -type Directory }

            IF(!(Test-Path C:\BDR\Collector)) { NEW-ITEM C:\BDR\Collector -type Directory }

            IF(!(Test-Path C:\BDR\Logs\EventViewer)) { New-Item C:\BDR\Logs\EventViewer -type Directory }

            IF(!(Test-Path C:\Temp)) { New-Item C:\Temp -type Directory }

            IF(!(Test-Path C:\BDR\ScheduledTasks)) { New-Item C:\BDr\ScheduledTasks -Type Directory }



            # Variables
                $doyenzdir = "DoyenzWorkSpace"

            # Check what drives exists
                $alldrives = gwmi win32_LogicalDisk -Filter DriveType=3 # DriveType 3 is HardDrives only

            # Create Functions

                function createdoyenzdir ($letter)
                    {
                        # Create Directory
                            IF(!(Test-Path "$letter\$doyenzdir"))
                                {
                                    New-Item "$letter\$doyenzdir" -Type Directory -ErrorAction SilentlyContinue
                                }

                    }

            # Test if already exist
            #    IF($alldrives.DeviceID -eq "D:")
            #        {

                                createdoyenzdir D:
            #        }
            #    ELSE
            #        {

                                createdoyenzdir F:

            #        }
            return $updatelogs
        }

    function scheduledtasks

        {
        # Update scheduled tasks =======================================================================================

            Echo "Cleaning up and setting correct scheduled tasks..."


        # HSR Monitor

            SchTasks /delete /TN "HeadStart Restore" /f
            SchTasks /create /SC hourly /mo 4 /sd 09/07/2012 /tn "HeadStart Restore" /TR C:\bdr\HSR\HSRMonitor.exe /RU SYSTEM /RL HIGHEST

        # Image Verification

            Echo "Deleting old legacy tasks..."
            SchTasks /delete /TN "End" /f
            SchTasks /delete /TN "Log" /f
            SchTasks /delete /TN "Login" /f
            SchTasks /delete /TN "Backup Monitor" /f
            SchTasks /delete /TN "Test Virtualization" /f
            SchTasks /delete /TN "Image Verification" /f
            Echo "Updating Image Verification task..."
			##Updated to run new ImageVerification wrapper script -- ZB -- 6/2/2015
            SchTasks /create /SC Daily /TN "Image Verification" /ST 18:00:00 /TR "powershell -file C:\installers\IVwrapper.ps1" /RU SYSTEM /RL HIGHEST


        # Update Script

            cd C:\BDR\ScheduledTasks
            downloadfile ScheduledTasks/BDRUpdate.xml C:\BDR\ScheduledTasks\BDRUpdate.xml
            downloadfile ScheduledTasks/BDRUpdate2.xml C:\BDR\ScheduledTasks\BDRUpdate2.xml

            SchTasks /delete /TN "BDRUpdate" /f
            Start-Sleep -Seconds 5
            SchTasks /create /TN "BDRUpdate" /xml "C:\BDR\ScheduledTasks\BDRUpdate.xml"
            #SchTasks /create /SC Daily /tn "BDRUpdate" /ST 01:11:00 /TR "powershell -file C:\installers\update.ps1" /RU SYSTEM /RL HIGHEST
            SchTasks /delete /TN "BDRUpdate2" /f
            Start-Sleep -Seconds 5
            SchTasks /create /TN "BDRUpdate2" /xml "c:\BDR\ScheduledTasks\BDRUpdate2.xml"

        # Setup PS Monitor Scheduled Task

            SchTasks /delete /TN "PSMON" /f
            SchTasks /create /SC Daily /tn "PSMON" /ST 02:11:00 /TR "powershell -file C:\installers\Monitor.ps1" /RU SYSTEM /RL HIGHEST

        # Install Support Popups

            cd C:\BDR\ScheduledTasks
            downloadfile ScheduledTasks/SupportHSR.xml C:\BDR\ScheduledTasks\SupportHSR.xml

        # Collector File Lock Fix

            cd C:\BDR\ScheduledTasks
            downloadfile ScheduledTasks/collectorrestart.xml C:\BDR\ScheduledTasks\collectorrestart.xml
            cd c:\Installers
            downloadfile Scripts/collectorrestart.ps1 C:\Installers\collectorrestart.ps1
            SchTasks /delete /TN "CollectorRestart" /f
            Start-Sleep -Seconds 5
            SchTasks /create /TN "CollectorRestart" /xml "C:\BDR\ScheduledTasks\collectorrestart.xml"


    }

    function ARRCDNS
        {
            Echo "Setting GoogleDNS for ARRC Systems..."

            $GETPARTNERID = Get-Content c:\BDR\serial.txt -ErrorAction SilentlyContinue

            IF($GETPARTNERID -eq "1001214")

              {
                    netsh interface ip add dns "Local Area Connection" 8.8.8.8 index=2

              } ELSE { Echo "Not an ARRC System" }
        }

    function unzip($filename)
        {
            $shell_app=new-object -com shell.application
            $zip_file = $shell_app.namespace((Get-Location).Path + "\$filename")
            $destination = $shell_app.namespace((Get-Location).Path)
            $destination.Copyhere($zip_file.items())
        }

    function unzip2($filename, $location) # unzip2 c:\temp\file.zip C:\newdir
        {
           # &'C:\Program Files\winrar x $filename $location
        }

    function updatescript
        {
                IF(!(Test-Path C:\Installers\update.ps1))
                    {

                        Echo "Downloading Update Script"
                        cd C:\Installers
                        downloadfile update.ps1 C:\Installers\update.ps1
                    }
                ELSE
                    {
                        # Test Size of Update Script to make sure its not empty
                            $updatesize = (Get-Item C:\Installers\update.ps1).Length

                            IF($updatesize -eq 0)
                                {
                                    Echo "Blank Update Script Detected!"
                                    Remove-Item C:\Installers\update.ps1 -Force -ErrorAction SilentlyContinue
                                    Echo "Downloading Update Script"
                                    cd C:\Installers
                                    downloadfile update.ps1 C:\Installers\update.ps1
                                }
                            ELSE
                                {
                                    Echo "Update Script appears to be correct"
                                }
                    }
        }


    function removelabtech

        {

   IF($GETPARTNERID -eq "1001214")

        {   } ELSE {


            IF (!(TEST-PATH HKLM:SOFTWARE\CharTec\LabTechRemoval))

            {
                IF (Get-Service LTService)

                    {
                        Stop-Service LTService
                        Set-Service LTService -StartupType Disabled
                    }
                ELSE

                    {
                        Echo "LTService doesn't exist or is not running..."
                    }

                IF (Get-Service LTSvcMon)

                    {
                        Stop-Service LTSvcMon
                        Set-Service LTSvcMon -StartupType Disabled
                    }
                ELSE

                    {
                        Echo "LTSvcMon doesn't exist or is not running..."
                    }


                # Set Registry as complete.

                    $RegSBDRShareFix = "HKLM:\SOFTWARE\CharTec"

                    New-Item -path $RegSBDRShareFix -name LabTechRemoval -value 1 -Force

                    SchTasks /delete /TN "Killlabtech" /f

            } ELSE { Echo "Labtech already disabled" }
        }

    }


    function removeplainkey

        {
            # Remove plainkey from C:\BDR\ directory
                IF (!(Test-Path c:\BDR\Plainkey.txt))
                    {} ELSE { Remove-Item c:\BDR\Plainkey.txt -force }
                IF (!(Test-Path c:\Users\Administrator\Desktop\Plainkey.txt))
                    {} ELSE { Remove-Item c:\Users\Administrator\Desktop\Plainkey.txt -force }
        }

    function updatehsr

        {
            $hsrver = (Get-Command C:\BDR\HSR\HSRMonitor.exe).FileVersionInfo.ProductVersion

            IF(!(Test-Path C:\BDR\HSR\HSRMonitor.exe))

                {
                    Echo "Downloading HSR Monitor"
                    cd c:\BDR\HSR
                    downloadfile HSR/HSRMonitor.exe c:\BDR\HSR\HSRMonitor.exe

                }


            IF($hsrver -eq "1.0.1.0")

            {   } ELSE {

                Echo "Updating HSR Monitor"
                cd c:\BDR\HSR
                downloadfile HSR/HSRMonitor.exe c:\BDR\HSR\HSRMonitor.exe
            }

        }

# =================================================================================================
# ImageVerification (Test Restore) Update RL 3/10/2014
# =================================================================================================

    function updateiv
        {
            # Verify Image Verification
                $files =  @("ImageVerification.exe",
                            "ImageVerification.sln",
                            "ImageVerification.suo",
                            "Telerik.WinControls.dll",
                            "Telerik.WinControls.GridView.dll",
                            "Telerik.WinControls.GridView.xml",
                            "Telerik.WinControls.UI.dll",
                            "TelerikCommon.dll")
                $IVversion = "1.4.0.0"

                foreach($file in $files)
                    {
                      $completefile = "C:\BDR\ImageVerification\$file"

                      if(!(Test-Path -Path $completefile -ErrorAction SilentlyContinue))
                        {
                            $updatelogs += "ImageVerification Missing Files      [$file]"
                            cd /BDR/ImageVerification
                            # Download File
                                $filedownload1 = "ImageVerification/$file"
                                $filedownload2 = "C:\BDR\ImageVerification\$file"
                                downloadfile $filedownload1 $filedownload2

                        }

                    }

                 # Verify Version of ImageVerification

                 # Check for Version
                    $IVver = (Get-Command C:\BDR\ImageVerification\ImageVerification.exe).FileVersionInfo.ProductVersion
                    if($IVver -ne $IVversion)
                        {
                            $updatelogs += "ImageVerification     [Update Needed]"
                            # Remove Old File
                                Remove-Item -Path C:\BDR\ImageVerification\ImageVerification.exe -Force -ErrorAction SilentlyContinue
                            # Download Update
                                downloadfile ImageVerification/ImageVerification.exe C:\BDR\ImageVerification\ImageVerification.exe
                        }

             echo "`n`n"
             Add-Content -Path "C:\BDR\Logs\BDRInfo\Update\UpdateScript-$timestamp3.txt" -Value "$updatelogs,"
             return $updatelogs
             echo "`n`n"
        }

# =================================================================================================
# Last updated 7-23-14 by ZJ Updated SP verstion to 5.2.0 and Image Manager to 6.5.4
# 	Added IVwrapper.ps1 download -- 6/1/15 -- ZB
# =================================================================================================

    function updateinstallerdirectory

        {
            Echo "Checking Updating Installer Directory..."

			# // ----------------------------------------------------------------------------------
			# // ScreenConnect
			# // ----------------------------------------------------------------------------------
				if (!(test-path C:\Installers\ScreenConnect.ClientSetup-ClassicBDR.msi)) {
					echo "Missing ScreenConnect Installer...";
					cd C:\Installers
					downloadfile Remote_Access_Agents/ScreenConnect.ClientSetup-ClassicBDR.msi C:\installers\ScreenConnect.ClientSetup-ClassicBDR.msi
				}
			# // ----------------------------------------------------------------------------------
			# // uAgent
			# // ----------------------------------------------------------------------------------
				if(!(test-path c:\installers\uAgent.exe))
					{
						echo "Missing uAgent Installer..."
						cd c:\installers
						downloadfile Utilities/uAgent.exe c:\installers\uAgent.exe
					}



           # Updated VirtualBox 4.3.2

                IF(!(TEST-PATH c:\installers\VirtualBox4.3.2.zip))
                    {
                        Echo "Missing Updated VirtualBox 4.3.2"
                        cd c:\installers
                        downloadfile VirtualBoot/VirtualBox4.3.2/VirtualBox4.3.2.zip c:\installers\VirtualBox4.3.2.zip
                    } Else {}

            # ShadowProtect Recovery Environment...

               <# IF(!(TEST-PATH c:\installers\ShadowProtect_RE_3.5.2.iso))
                    {
                        Echo "Missing ShadowProtect Recovery Environment 3.5.2, Downloading now..."
                        cd C:\Installers
                        downloadfile StorageCraft_RecoveryEnvironment/ShadowProtect_RE_3.5.2.iso c:\installers\ShadowProtect_RE_3.5.2.iso

                    } Else { } #>

                IF(!(Test-Path c:\installers\ShadowProtect_RE_5.0.4.iso))
                    {
                        Echo "Missing ShadowProtect Recovery Environment 5.0.4, Downloading now..."
                        cd C:\Installers
                        downloadfile StorageCraft_RecoveryEnvironment/ShadowProtect_RE_5.0.4.iso c:\installers\ShadowProtect_RE_5.0.4.iso

                    }
                Else
                    {
                        $spresize = (Get-Item c:\installers\ShadowProtect_RE_5.0.4.iso).Length
                        IF($spresize -eq 0)
                            {
                                downloadfile StorageCraft_RecoveryEnvironment/ShadowProtect_RE_5.0.4.iso c:\installers\ShadowProtect_RE_5.0.4.iso
                            }
                        ELSE
                            {
                                Remove-Item c:\Installers\ShadowProtect_RE_5.0.3.iso -Force -ErrorAction SilentlyContinue
                                Remove-Item c:\Installers\ShadowProtect_RE_5.0.1.iso -Force -ErrorAction SilentlyContinue
                                Remove-Item c:\Installers\ShadowProtect_RE_5.0.0.iso -Force -ErrorAction SilentlyContinue
                                Remove-Item c:\Installers\ShadowProtect_RE_4.2.7.iso -Force -ErrorAction SilentlyContinue

                            }
                    }

            # ShadowProtect...

                IF(!(Test-Path C:\installers\ShadowProtectSetup_MSP_5.2.3.exe))
                    {
                        Echo "Missing ShadowProtect 5.2.3 Installer"
                        cd C:\Installers
                        downloadfile ShadowProtectSetup_MSP_5.2.3.exe c:\installers\ShadowProtectSetup_MSP_5.2.3.exe

                        Echo "Copying to BDR Directory for new servers... (sp.exe)"
                        Copy-Item c:\installers\ShadowProtectSetup_MSP_5.2.3.exe C:\BDR\Scripts\sp.exe -Force

                    } Else {

                        # Get ShadowProtect Version
                            $spver = (Get-Command C:\BDR\scripts\sp.exe).FileVersionInfo.ProductVersion

                        # Verify Correct File Size

                        Remove-Item C:\Installers\ShadowProtectSetup_MSP_5.0.2.exe -Force -ErrorAction SilentlyContinue
                        Remove-Item C:\Installers\ShadowProtectSetup_MSP_5.0.1.exe -Force -ErrorAction SilentlyContinue
                        Remove-Item C:\Installers\ShadowProtectSetup_MSP_5.0.0.exe -Force -ErrorAction SilentlyContinue
                        Remove-Item C:\Installers\ShadowProtectSetup_MSP_4.2.7.exe -Force -ErrorAction SilentlyContinue

                        # 5.2.0.36537

                        if(!(Test-Path C:\BDR\scripts\sp.exe))
                            {
                                Echo "Copying SP.exe to C:\BDR\Scripts\"
                                Copy-Item c:\installers\ShadowProtectSetup_MSP_5.2.3.exe C:\BDR\Scripts\sp.exe -Force
                            }
                    }

                IF(!(Test-Path C:\Installers\ShadowProtectSetup_MSP_5.2.0.iss))
                    {
                        Echo "Missing ShadowProtect Install Answer File..."
                        cd C:\installers
                        downloadfile ShadowProtectSetup_MSP_5.2.0.iss c:\installers\ShadowProtectSetup_MSP_5.2.0.iss
                    }

            # -------------------------------------------------------------------------------------
            # Image Manager 2016-01-04 RL
            # -------------------------------------------------------------------------------------
                if(!(test-path c:\installers\ImageManager_Setup_6.7.7.exe))
                    {
                        echo "Missing ImageManager 6.7.7, downloading now..."
                        cd C:\installers
                        downloadfile ImageManager/ImageManager_Setup_6.7.7.exe C:\installers\ImageManager_Setup_6.7.7.exe
                    }
            # -------------------------------------------------------------------------------------
            #
            # -------------------------------------------------------------------------------------
<#  Disabled By Roger 2016-01-04

               # ImageManager

                IF(!(Test-Path c:\installers\ImageManager-6-Setup.exe))
                    {
                        Echo "Missing ImageManager6, Downloading now..."
                        cd C:\Installers
                        downloadfile ImageManager-6-Setup.exe c:\installers\ImageManager-6-Setup.exe
                    }
                ELSE
                    {
                        # Test Version
                            $imver = (Get-Command C:\installers\ImageManager-6-Setup.exe).FileVersionInfo.ProductVersion

                        # Remove WhiteSpace from Variable
                            $imver2 = $imver -replace " ", ""

                            IF($imver2 -eq "6.5.4")
                                {}
                            ELSE
                                {
                                    Echo "Newer version of ImageManager Detected..."
                                    Remove-Item C:\Installers\ImageManager-6-Setup.exe -Force
                                    cd c:\installers
                                    downloadfile ImageManager-6-Setup.exe c:\installers\ImageManager-6-Setup.exe
                                }

                    }
#>

            # Intel Raid Tool

                IF(!(Test-Path C:\Installers\CmdTool2.exe))
                    {
                        Echo "Missing CMDTool2, Downloading now..."
                        cd C:\Installers
                        downloadfile Utilities/CmdTool2.exe c:\installers\CmdTool2.exe
                    }

            # Download Monitor Script

                IF(!(Test-Path C:\Installers\Monitor.ps1))
                    {
                        Echo "PS Monitor downloading it now..."
                        cd C:\Installers
                        downloadfile Scripts/Monitor.ps1 c:\installers\Monitor.ps1
                    }
                ELSE
                    {
                        # Test Size of Update Script to make sure its not empty
                            $monitorsize = (Get-Item C:\Installers\Monitor.ps1).Length

                            IF($monitorsize -eq 0)
                                {
                                    Remove-Item C:\Installers\Monitor.ps1 -Force
                                    Echo "PS Monitor downloading it now..."
                                    cd C:\Installers
                                    downloadfile Scripts/Monitor.ps1 c:\installers\Monitor.ps1
                                }
                            ELSE
                                {
                                    Remove-Item C:\Installers\Monitor.ps1 -Force
                                    Echo "PS Monitor downloading it now..."
                                    cd C:\Installers
                                    downloadfile Scripts/Monitor.ps1 c:\installers\Monitor.ps1
                                }
                    }


            # Process Monitor

                IF(!(Test-Path C:\installers\ProcessMonitor.zip))
                    {
                        Echo "Missing ProcessMonitor, downloading now..."
                        cd C:\Installers
                        downloadfile Utilities/ProcessMonitor.zip c:\installers\ProcessMonitor.zip
                    }

            # PSEXEC

                IF(!(Test-Path C:\installers\psexec.exe))
                    {
                        Echo "Missing PSEXEC.exe, downloading now.."
                        cd C:\Installers
                        downloadfile Utilities/psexec.exe c:\installers\psexec.exe
                    }

            # RAMMAP

                IF(!(Test-Path c:\installers\RAMMap.exe))
                    {
                        Echo "Missing RAMMap.exe, downloading now..."
                        cd C:\Installers
                        downloadfile Utilities/RAMMap.exe c:\installers\RAMMap.exe
                    }

            # TeraCopy

                IF(!(Test-Path c:\installers\teracopy227.exe))
                    {
                        Echo "Missing Teracopy227, downloading now..."
                        cd C:\Installers
                        downloadfile Utilities/teracopy227.exe c:\installers\teracopy227.exe
                    }


            # Vipre

                IF(!(Test-Path C:\installers\Vipre-6.5-2014-08-05.EXE))
                    {
                        Echo "Missing Vipre, downloading now..."
                        cd C:\Installers
                        downloadfile Vipre-6.5-2014-08-05.EXE C:\Installers\Vipre-6.5-2014-08-05.EXE
                    }

            # Doyenz Agent

                IF(!(Test-Path C:\installers\DoyenzAgentInstaller.exe))
                    {
                        Echo "Missing Doyenz, downloading now..."
                        cd C:\Installers
                        downloadfile DoyenzAgentInstaller.exe c:\installers\DoyenzAgentInstaller.exe

                        IF(!(Test-Path 'C:\Program Files (x86)\Doyenz\Agent\Updater\InitialVersion\DoyenzAgentUpdater.exe'))
                            {
                                (Start-Process -FilePath "C:\Installers\DoyenzAgentInstaller.exe" -ArgumentList "/S /RUNSERVICE=YES /RUNTRAYMONITOR=no" -Wait -PassThru).ExitCode
                            }
                        ELSE {}
                    }
                ELSE
                    {
                        Echo "Testing Doyenz Version..."
                        $doyenzver = (Get-Command C:\installers\DoyenzAgentInstaller.exe).FileVersionInfo.FileVersion

                        IF($doyenzver -ne "3.3.8.10964")
                            {
                                Remove-Item "C:\Installers\DoyenzAgentInstaller.exe" -Force
                                cd c:\Installers
                                downloadfile DoyenzAgentInstaller.exe c:\installers\DoyenzAgentInstaller.exe
                            }
                        ELSE
                            {

                                IF(!(Test-Path 'C:\Program Files (x86)\Doyenz\Agent\Updater\InitialVersion\DoyenzAgentUpdater.exe'))
                                    {
                                        (Start-Process -FilePath "C:\Installers\DoyenzAgentInstaller.exe" -ArgumentList "/S /RUNSERVICE=YES /RUNTRAYMONITOR=no" -Wait -PassThru).ExitCode
                                    }
                                ELSE {}

                            }

                    }

                IF(!(Test-Path C:\installers\spdiagnostic_7.0.198.zip))
                    {
                        Echo "Missing SP Diagnostics, downloading now..."
                        cd C:\Installers
                        downloadfile Utilities/spdiagnostic_7.0.198.zip c:\installers\spdiagnostic_7.0.198.zip
                    }

                IF(!(Test-Path c:\installers\VSSFix.bat))
                    {
                        Echo "Missing VSS Fix script, downloading now..."
                        cd C:\Installers
                        downloadfile Scripts/VSSFix.bat c:\installers\VSSFix.bat
                    }
            # Wget

                IF(!(Test-Path c:\Installers\wget.exe))
                    {
                        Echo "Missing WGET, downloading now..."
                        cd c:\installers
                        downloadfile Utilities/wget.exe c:\installers\wget.exe
                    }

            # .NET4.0 Installer

                IF(!(Test-Path C:\Installers\dotNetFx40_Full_x86_x64.exe))
                    {
                        Echo "Missing .net 4 installers..."
                        cd \installers
                        downloadfile Required_Software_Packages/dotNetFx40_Full_x86_x64.exe C:\installers\dotNetFx40_Full_x86_x64.exe
                    }

            # BDRService Installer
                if(!(Test-Path C:\Installers\BDRServiceLoader.exe))
                    {
                        cd C:\Installers
                        downloadfile BDRServiceAgent/BDRAgent/BDRServiceloader.exe C:\Installers\BDRServiceLoader.exe

                    }
            # BDRWatchdogService Installer
                if(!(Test-Path C:\Installers\BDRWatchdogServiceSetup.msi))
                    {
                        cd C:\Installers
                        downloadfile BDRServiceAgent/BDRAgent/BDRWatchdogServiceSetup.msi C:\Installers\BDRWatchdogServiceSetup.msi

                    }



            # Getserials.ps1
                if(!(Test-Path C:\Installers\getserials.ps1))
                    {
                        cd C:\installers
                        downloadfile Scripts/getserials.ps1 C:\Installers\getserials.ps1


                    }



			# Image Verification PowerShell Wrapper Script -- Added 6/1/2015 -- ZB
			if(!(Test-Path C:\Installers\IVwrapper.ps1)){
				echo "Missing IVwrapper script! Downloading..."
				cd C:\installers
				downloadfile IVwrapper.ps1 C:\installers\IVwrapper.ps1
			} else {
				$ivwrappersizecheck = (Get-Item C:\installers\IVwrapper.ps1).Length
				if($ivwrappersizecheck -lt 1000){
					echo "IVwrapper script is wrong size! Redownloading..."
					cd C:\installers
					downloadfile IVwrapper.ps1 C:\installers\IVwrapper.ps1
				}
			}

			# Reset-IM script
			if(!(Test-Path C:\installers\Reset-IM-1-1.bat)){
				cd C:\installers
				downloadfile Scripts/Reset-IM-1-1.bat C:\installers\Reset-IM-1-1.bat
			} else {
				#Remove old versions
				$oldRIMvers = @("C:\installers\Reset-IM-0-3.bat",
					"C:\installers\Reset-IM.bat",
					"C:\installers\Reset_IM.bat",
					"C:\Users\arrcnetadmin\Downloads\Reset_IM.0.3.bat",
					"C:\installers\Reset-IM-1-0.bat")
				foreach ($oldRIMver in $oldRIMvers) {
					if(Test-Path $oldRIMver){
						Remove-Item $oldRIMver -Force -ErrorAction SilentlyContinue
					}
				}
			}


        }

    function updatebdrdirectory

        {

            # Production / Decrypt Key

               $productiondir = (Get-ChildItem C:\BDR\Production).Count

               IF($productiondir -gt 10)
                    { } Else {

                        Echo "Missing Production directory files, downloading now..."
                        cd c:\temp
                        downloadfile Deployment/Production.zip c:\temp\Production.zip

                        Start-Sleep -seconds 10
                        cd /temp
                        unzip Production.zip
                        Copy-Item C:\Temp\Production\*.* c:\BDR\Production -force

                        Remove-Item C:\Temp\Production -Recurse -Force

                    }
            # Collector

                $collectordir = (Get-ChildItem C:\BDR\Collector).Count

                IF($collectordir -gt 10)
                    { } Else {

                        Echo "Missing Collector Directory Files, downloading now..."
                        cd c:\temp
                        downloadfile Deployment/Collector.zip c:\temp\Collector.zip

                        Start-Sleep -seconds 10
                        cd /temp
                        unzip Collector.zip
                        Copy-Item C:\temp\collector\*.* c:\bdr\Collector -force -ErrorAction SilentlyContinue

                        Remove-Item c:\temp\collector -Recurse -Force
                    }

        }



    function remoteaccess

        {

            # LogMeIn
                IF(!(Get-Service "LogMeIn" -ErrorAction SilentlyContinue))
                    {
                        IF(!(TEST-PATH C:\installers\CT-Logmein.exe))
                            {
                                Echo "Missing LogMeIn Installer, downloading now..."
                                cd C:\Installers
                                downloadfile Remote_Access_Agents/CT-Logmein.exe c:\installers\CT-Logmein.exe

                            } ELSE {

                                Echo "LogMeIn is already downloaded... Installing now"

                                $lmifile = 'C:\installers\CT-Logmein.exe'

                                Start-Process -FilePath $lmifile -PassThru | Wait-Process

                            }

                    } ELSE { }


            # TeamViewer
                $tvfile = 'C:\installers\CT-TeamViewer.exe'
                IF(!(Get-Service "TeamViewer8" -ErrorAction SilentlyContinue))
                    {
                        IF(!(Test-Path c:\installers\CT-TeamViewer.exe))
                            {
                                Echo "Missing TeamViewer Installer, downloading now..."
                                cd c:\installers
                                downloadfile Remote_Access_Agents/CT-TeamViewer.exe C:\installers\CT-TeamViewer.exe
                                Start-Sleep -Seconds 10

                            } ELSE {

                                Echo "TeamViwer Installer Found... Installing Now..."
                                Start-Process -FilePath $tvfile -PassThru | Wait-Process


                                # Write update to Registry

                                $teamViewer8 = "HKLM:\SOFTWARE\CharTec"

                                New-Item -path $teamViewer8 -name TeamViewer8 -value 1 -Force

                            }


                       } ELSE { }
    }

    function documents

        {
         # General Documents (Agnostic of HW SW or ESXi HV BDR) ==========================================================

            # BDR Setup Guide...
            Echo "Checking for BDR Setup Guide..."

            IF(!(TEST-PATH c:\Documentation\BDR_Setup.pdf))
                {
                    Echo "Downloading BDR Setup Guide..."
                    cd C:\Documentation
                    downloadfile Documents/Hardware/BDR_Setup.pdf  c:\Documentation\BDR_Setup.pdf
                }

            # BDR Virtualization Guide...
            Echo "Checking for Virtualization Guide..."

            IF(!(TEST-PATH c:\Documentation\VirtualizationGuide_v3.pdf))
                {
                    Echo "Downloading Virtualization Guide..."
                    cd C:\Documentation
                    downloadfile Documents/VirtualizationGuide_v3.pdf C:\Documentation\VirtualizationGuide_v3.pdf
                }

        # SoftBDR ONLY  ============================================================================================

            # SaaSBDR Guide...
            Echo "Checking if this is a SoftBDR..."

            $ver = (Get-Item "HKLM:SOFTWARE\Microsoft\Windows NT\CurrentVersion").GetValue("ProductName")

              if($ver -eq "Windows 7 Professional")
                {
                    $BDR = "SBDR"
                    Echo "SoftBDR detected..."

                    if(!(TEST-PATH c:\Documentation\SaaS_BDR.pdf))
                        {
                            Echo "Downloading BDR SaaS Guide..."
                            cd C:\Documentation
                            downloadfile Documents/Hardware/SaaS_BDR.pdf C:\Documentation\SaaS_BDR.pdf
                        }
                }

            # Create Desktop Shortcut to C:\Documentation
                $Users = Get-ChildItem C:\Users -Name
            # Old Documents to be removed
                $OldDesktopDocs = @("BDR Setup.pdf",
                                    "BDR Setup - Shortcut.lnk",
                                    "Virtualization Guide.pdf")

               Foreach($User in $Users)
                  {

                    # Create ShortCut to latest Documents
                        $WshShell = New-Object -comObject WScript.Shell
                        $Shortcut = $WshShell.CreateShortcut("C:\Users\$User\Desktop\BDR Guides.lnk")
                        $Shortcut.TargetPath = "C:\Documentation"
                        $Shortcut.Save()

                    # Remove Old Documents

                        Foreach($OldDesktopDoc in $OldDesktopDocs)
                            {

                                IF(Test-Path "C:\Users\$User\Desktop\$OldDesktopDoc")
                                    {
                                        Remove-Item "C:\Users\$User\Desktop\$OldDesktopDoc" -Force
                                    }
                            }
                  }

            # Old Document Removal
                $OldDocs = @("c:\Documentation\Virtualization_Guide.pdf")

                Foreach($OldDoc in $OldDocs)
                    {
                        IF(Test-Path $OldDoc)
                            {
                                Remove-Item $OldDoc -Force -ErrorAction SilentlyContinue
                            }
                    }

    }

    function fixes

        {
                # Fix SaaSBDR Share issues -- ADDED to ALL BDRs to combat shares notbeing closed out and using up all BDR memory resources Feb 26, 2013

                  IF (!(TEST-PATH HKLM:SOFTWARE\CharTec\SBDRShareFix))

                      {
                            Echo "Missing Auto Disconnect Fix"
                            CMD /C 'net config server /autodisconnect:03'

                            $RegSysCache = "HKLM:\System\CurrentControlSet\Control\Session Manager\Memory Management"
                            $RegLanman   = "HKLM:\SYSTEM\CurrentControlSet\Services\LanmanServer"

                            Set-ItemProperty -path $RegSysCache -name LargeSystemCache -value 1 -Force
                            Set-ItemProperty -path $RegLanman -name Parameters -value 3 -Force

                            Echo "Setting SBDR Share to fixed..."

                            $RegSBDRShareFix = "HKLM:\SOFTWARE\CharTec"

                            New-Item -path $RegSBDRShareFix -name SBDRShareFix -value 1 -Force


                     } ELSE {

                     }

                    # Fix Missing text from Buttons on ShadowProtect 5.0.3

                        IF(!(Test-Path HKLM:SOFTWARE\CharTec\SP503MissingTXTfix))

                            {
                                Echo "Missing ShadowProtect 5.0.3 Missing TXT fix"
                                Echo "Checking ShadowProtect Version..."

                                    $shadowprotectver = (Get-Command "C:\Program Files (x86)\StorageCraft\ShadowProtect\ShadowProtect.exe").FileVersionInfo.ProductVersion

                                    IF($shadowprotectver -ne "5.0.3.26070")

                                        {
                                            Echo "Fix not needed"
                                        }
                                    ELSE
                                        {
                                            Echo "Applying Fix"

                                                IF(Test-Path C:\Installers\ShadowProtectSetup_MSP_5.0.3.exe)

                                                    {
                                                        $CharTecREG = "HKLM:\SOFTWARE\CharTec"


                                                        # Test Size
                                                        $spsize = (Get-Item c:\installers\ShadowProtectSetup_MSP_5.0.3.exe).Length

                                                            IF($spsize -eq 93728008)
                                                                {
                                                                    Echo "Repairing ShadowProtect 5.0.3"
                                                                    cd c:\installers

                                                                    (Start-Process -FilePath "ShadowProtectSetup_MSP_5.0.3.exe" -ArgumentList "Install LANG=en" -Wait -PassThru).ExitCode

                                                                    New-Item -path $CharTecREG -name SP503MissingTXTfix -value 1 -Force

                                                                    cd c:\installers
                                                                    ./Monitor.ps1
                                                                }
                                                            ELSE
                                                                {
                                                                    Remove-Item C:\installers\ShadowProtectSetup_MSP_5.0.3.exe -Force
                                                                    downloadfile ShadowProtectSetup_MSP_5.0.3.exe c:\installers\ShadowProtectSetup_MSP_5.0.3.exe

                                                                    Echo "Repairing ShadowProtect 5.0.3"
                                                                    cd c:\installers

                                                                    (Start-Process -FilePath "ShadowProtectSetup_MSP_5.0.3.exe" -ArgumentList "Install LANG=en" -Wait -PassThru).ExitCode

                                                                    New-Item -path $CharTecREG -name SP503MissingTXTfix -value 1 -Force

                                                                    cd c:\installers
                                                                    ./Monitor.ps1

                                                                }

                                                    }
                                            }
                                }


        }

    function webbmc

        {
            # =====================================================================================================================
            #
            #                                 Apache / PHP / PHP VirtualBox Install
            #
            # =====================================================================================================================


            IF(!(Get-Service apache2.4 -ErrorAction SilentlyContinue))

            {

            # Download Package

                cd c:\temp
                downloadfile Deployment/Apache24.zip c:\temp\Apache24.zip

            # Unzip Package

                IF(!(Test-Path C:\temp\Apache24.zip))

                    {
                        Echo "Waiting for Apache24.zip to complete download..."
                        Start-Sleep -Seconds 15

                        IF(!(Test-Path C:\Temp\Apache24.zip))

                            {
                                Echo "Still waiting for Apache24.zip to complete download..."
                                Start-Sleep -Seconds 15

                                cd c:\temp
                                unzip Apache24.zip
                            }
                        ELSE
                            {
                                cd c:\temp
                                unzip Apache24.zip
                            }
                    }
                ELSE
                    {
                        cd c:\temp
                        unzip Apache24.zip
                    }

            # Move Directory

                Move-Item C:\Temp\Apache24 C:\Apache24 -Force
                Copy-Item c:\Apache24\PHP\php.ini c:\Windows\php.ini -Force

            # Detect C++ Redistributable

                # IF(!(Test-Path HKLM:\SOFTWARE\Microsoft\VisualStudio\10.0))
                IF(!(Test-Path "C:\Windows\System32\MSVCR100.dll"))
                    {
                        cd c:\temp
                        downloadfile Required_Software_Packages\vcredist_x64.exe c:\temp\vcredist_x64.exe
                        Start-Sleep -Seconds 10

                        IF(Test-Path c:\temp\vcredist_x64.exe)
                            {
                                Move-Item C:\Temp\vcredist_x64.exe "C:\Program Files\vcredist_x64.exe" -force

                                IF(Test-Path "C:\Program Files\vcredist_x64.exe")

                                    {
                                        cd 'C:\Program Files'
                                        $visinstall = 'vcredist_x64.exe'
                                        $visarg = '/q'

                                        (Start-Process -FilePath $visinstall -ArgumentList $visarg -PassThru -Wait).ExitCode
                                    }
                            }


            # Create Services

            #Apache

                $apacheinstall = 'C:\apache24\bin\httpd.exe'
                $apachearg     = '-k install'

                Start-Process -FilePath $apacheinstall -ArgumentList $apachearg -PassThru | Wait-Process

                #Start-Service apache2.4


        } ELSE {

            # Create Services

            #Apache
                $apacheinstall = 'C:\apache24\bin\httpd.exe'
                $apachearg     = '-k install'

                Start-Process -FilePath $apacheinstall -ArgumentList $apachearg -PassThru | Wait-Process

                #Start-Service apache2.4
        }

    IF(!(Test-Path c:\Installers\Oracle_VM_VirtualBox_Extension_Pack-4.0.4-70112.vbox-extpack))

        {

            cd C:\Installers
            downloadfile VirtualBoot\Oracle_VM_VirtualBox_Extension_Pack-4.0.4-70112.vbox-extpack c:\installers\Oracle_VM_VirtualBox_Extension_Pack-4.0.4-70112.vbox-extpack

        }
        }

# =====================================================================================================================
#
#                               End Apache / PHP / PHP VirtualBox Install
#
# =====================================================================================================================
        }



# =================================================================================================
#  MySQL 5.6 Install/ PHP / PhpMyAdmin
# =================================================================================================
    function mysql

        {
            # Push Latest PHP.ini to C:\Apache24\PHP\
                downloadfile Required_Software_Packages\php.ini C:\Apache24\PHP\php.ini
            # Restart apache Service so that php.ini will take effect.
                Restart-Service "Apache2.4"
                Start-Sleep -Seconds 5

            # Check if already installed
                 if(!(Get-Service "MySQL 5.6" -ErrorAction SilentlyContinue))
                     {
                       $updatelogs += "MySQL Service [Missing]"

                          # MySQL 5.6 Install
                               if(test-path "C:\temp\MySQLServer5.6.zip" -ErrorAction SilentlyContinue)
                                 {
                                    $updatelogs += "MySQL Service [Installing]"
                                    # Extract
                                        cd "C:\Temp"
                                        unzip MySQLServer5.6.zip
                                        cd "c:\temp\MySQL Server 5.6\"

                                    # Create Directories
                                       New-Item -Type Directory "C:\BDR\MySQL\" -ErrorAction SilentlyContinue

                                    # Copy
                                       $SQLFrom = "C:\Temp\MySQL Server 5.6"
                                       $SQLTo   = "C:\BDR\MySQL\MySQL Server 5.6"
                                       Copy-Item $SQLFrom $SQLTo -Force -Recurse

                                    # Install
                                       cd "C:\BDR\MySQL\MySQL Server 5.6\bin\"
                                       CMD /C 'mysqld  --install "MySQL 5.6" --defaults-file="C:\BDR\MySQL\MySQL Server 5.6\mysql.ini"'


                                     # Start Service
                                       if(Get-Service -Name "MySQL 5.6" -ErrorAction SilentlyContinue)
                                            {
                                               $updatelogs += "MySQL Service [Installed]"
                                                Start-Service "MySQL 5.6"
                                            }
                                        else
                                            {
                                               $updatelogs += "MySQL Service [Failed Install]"
                                            }
                                }
                            else
                                {
                                    # Download
                                        downloadfile 'Required_Software_Packages/MySQLServer5.6.zip' 'C:\Temp\MySQLServer5.6.zip'
                                }
                    }
                else
                    {
                        $updatelogs += "MySQL Service [OK]"
                    }

            # phpMyAdmin
                if(!(Test-Path C:\Apache24\htdocs\phpMyAdmin\ -ErrorAction SilentlyContinue))
                    {
                        $updatelogs += "phpMyAdmin [Missing]"
                        # Download
                            downloadfile 'Required_Software_Packages/phpMyAdmin.zip' 'C:\Temp\phpMyAdmin.zip'

                        # Extract
                            cd "C:\Temp"
                            unzip phpMyAdmin.zip
                            cd "C:\Temp\phpMyAdmin\"

                        # Copy
                            $phpadminFrom = "C:\Temp\phpMyAdmin"
                            $phpadminTo   = "C:\Apache24\htdocs\phpMyAdmin"
                            Copy-Item $phpadminFrom $phpadminTo -Force -Recurse
                    }
                 else
                    {
                        $updatelogs += "phpMyAdmin [OK]"
                    }
            echo "`n`n"
            Add-Content -Path "C:\BDR\Logs\BDRInfo\Update\UpdateScript-$timestamp3.txt" -Value "$updatelogs,"
            return $updatelogs
            echo "`n`n"
        }

# =================================================================================================
#  END ******************************************************** MySQL 5.6 Install/ PHP / PhpMyAdmin
# =================================================================================================
# =================================================================================================
#  Update ShadowProtect on the BDR     updated 7-23-14 by ZJ to version 5.2.0
# =================================================================================================

    function updatebdrshadowprotect

        {
            $currentspver = "5.2.3.37285"
            $shadowprotectver = (Get-Command "C:\Program Files (x86)\StorageCraft\ShadowProtect\ShadowProtect.exe").FileVersionInfo.ProductVersion

            IF($shadowprotectver -ne $currentspver)

                {
                    Echo "New ShadowProtect Version Detected..." $currentspver

                    IF(Test-Path C:\Installers\ShadowProtectSetup_MSP_5.2.3.exe)

                        {

                            # Test Size
                                $spsize = (Get-Item c:\installers\ShadowProtectSetup_MSP_5.2.3.exe).Length

                                IF($spsize -eq 126623464)
                                    {
                                        Echo "Updating BDR ShadowProtect..."
                                        cd c:\installers

                                        (Start-Process -FilePath "ShadowProtectSetup_MSP_5.2.3.exe" -ArgumentList "INSTALL LANG=en IACCEPT=STORAGECRAFT.EULA" -Wait -PassThru).ExitCode

                                        cd c:\installers
                                        ./Monitor.ps1
                                    }
                               ELSEIF ($spsize -ne 126623464)
                                    {
                                        Remove-Item C:\installers\ShadowProtectSetup_MSP_5.2.3.exe -Force
                                        downloadfile ShadowProtectSetup_MSP_5.2.3.exe c:\installers\ShadowProtectSetup_MSP_5.2.3.exe

                                        Echo "Updating BDR ShadowProtect..."
                                        cd c:\installers

                                        (Start-Process -FilePath "ShadowProtectSetup_MSP_5.2.3.exe" -ArgumentList "INSTALL LANG=en IACCEPT=STORAGECRAFT.EULA" -Wait -PassThru).ExitCode

                                        cd c:\installers
                                        ./Monitor.ps1

                                    }

                        }
                }
        }

function vmwareremoval

    {
        # Detection and removal of VMWare Server or VMware Player


        # VMWware Server

            IF(Test-Path 'C:\Program Files (x86)\VMware\VMware Server\vmrun.exe')

                {
                    Echo "VMware Server detected... Uninstalling now..."

                    (Start-Process -FilePath "wmic" -ArgumentList "product where name='VMware Server' call uninstall" -Wait -PassThru).ExitCode

                }
            ELSE { }

        # VMware Player

            IF(Test-Path 'C:\Program Files (x86)\VMware\VMware Player\vmplayer.exe')

                {

                    # Check if VM is running before uninstall
                        Echo "Checking for Running VM (VMware Player)"

                        IF(!(Get-Process vmplayer -ErrorAction SilentlyContinue))

                            {
                                # Uninstall 64 and 32 Bit  version
                                   Echo "VMware Player detected... Uninstalling now..."

                                        (Start-Process -FilePath "wmic" -ArgumentList "product where name='VMwarePlayer_x64' call uninstall" -Wait -PassThru).ExitCode
                                        (Start-Process -FilePath "wmic" -ArgumentList "product where name='VMware Player' call uninstall" -Wait -PassThru).ExitCode

                            }
                        ELSE
                            { Echo "VM was detected skipping uninstall" }
                }
            ELSE { }


    }

function disablefirewall

    {
        # returns true if windows firewall is enabled, false if it is disabled
        filter global:get-firewallstatus ([string]$computer = $env:computername)
	        {
	            if ($_) { $computer = $_ }

	            $HKLM = 2147483650

	            $reg = get-wmiobject -list -namespace root\default -computer $computer | where-object { $_.name -eq "StdRegProv" }
	            $firewallEnabled = $reg.GetDwordValue($HKLM, "System\ControlSet001\Services\SharedAccess\Parameters\FirewallPolicy\DomainProfile","EnableFirewall")

	            [bool]($firewallEnabled.uValue)
	        }

        IF(get-firewallstatus -eq "True")
            {
                Echo "Firewall is enabled"
                netsh advfirewall set allprofiles state off
            }
        ELSE
            {

            }

    }

function php

    {
        # ImageManager Settings (Version 6 + )
            downloadfile Scripts\imUpdate.php c:\Apache24\htdocs\imUpdate.php

        # Test ImageManager Version (Installed)
            $imagemanagerver = (Get-Command "C:\Program Files (x86)\StorageCraft\ImageManager\ImageManager.Client.exe").FileVersionInfo.ProductVersion

        # Run the following on 6.x +
            IF($imagemanagerver -gt 6)

                {

                    # Run ImageManager Settings update
                        (Start-Process -FilePath C:\Apache24\PHP\php.exe -ArgumentList "C:\Apache24\htdocs\imUpdate.php" -Wait -PassThru).ExitCode
                }


    }

    function disablehsr

        {

# Get ImageManager Version
    $imagemanagerver = (Get-Command "C:\Program Files (x86)\StorageCraft\ImageManager\ImageManager.Client.exe").FileVersionInfo.ProductVersion
# Test if ESXi
    $BDRType = Get-WmiObject Win32_Computersystem

# Check BDR for ESXi (VMWare Tools)

    IF($BDRType.Model -eq "VMware Virtual Platform")
        { Echo "ESXi BDR" }
    ELSE
        {

            IF($imagemanagerver -eq "5.0.5.15823")
                {

                    # ImageManager 5.0.5

                        $key = "HKLM:\SYSTEM\CurrentControlSet\Services\ShadowControl ImageManager\Parameters\Watches\"
                        $headstart = "\Headstart"
                        $ComponentKeys=Get-ChildItem -path $key -Name



                    foreach ($ComponentKey in $ComponentKeys)
                        {
                            $full = $key + $ComponentKey + $headstart

                            Remove-Item $full -Recurse -Force

                         }

                    # Restart ImageManager Service

                        Stop-Service "ShadowControl ImageManager" -Force

                        Sleep -Seconds 5

                        Start-Service "ShadowControl ImageManager"


                }
            ELSEIF($imagemanagerver -eq "5.0.1.13615")
                {

                    # ImageManager 5.0.1

                        $key = "HKLM:\SYSTEM\CurrentControlSet\Services\ShadowControl ImageManager\Parameters\Watches\"
                        $headstart = "\Headstart"
                        $ComponentKeys=Get-ChildItem -path $key -Name



                        foreach ($ComponentKey in $ComponentKeys)
                            {
                                $full = $key + $ComponentKey + $headstart

                                Remove-Item $full -Recurse -Force

                            }

                    # Restart ImageManager Service

                        Stop-Service "ShadowControl ImageManager" -Force

                        Sleep -Seconds 5

                        Start-Service "ShadowControl ImageManager"
                 }
            ELSEIF($imagemanagerver -eq "4.1.5.10017")
                {
                    # ImageManager 4.1.5

                        $key = "HKLM:\SYSTEM\CurrentControlSet\Services\ShadowProtect ImageManager\Parameters\Watches\"
                        $headstart = "\Headstart"
                        $ComponentKeys=Get-ChildItem -path $key -Name



                        foreach ($ComponentKey in $ComponentKeys)
                            {
                                 $full = $key + $ComponentKey + $headstart

                                 Remove-Item $full -Recurse -Force

                             }

                      # Restart ImageManager Service

                         Stop-Service "ShadowProtect ImageManager" -Force

                          Sleep -Seconds 5

                          Start-Service "ShadowProtect ImageManager"
                 }
            ELSE { Echo "Version 6 Detected" }

        }







        }

 function cleaninstallerdir

    {
        # Removes folders in C:\Installers Dir that are created during server install

            $serverdir = "C:\BDR\Server"
            $dir   = "C:\Installers\"

            $files = Get-ChildItem $serverdir -Filter "*.txt" | Where-Object {$_.PSIsContainer -eq $false} | Select-Object basename


            foreach ($file in $files)
                {

                    $serverfolder = $dir + ($file).BaseName

                    #Echo $serverfolder

                    Remove-Item $serverfolder -Recurse -Force -ErrorAction SilentlyContinue
                }

        # Files to Remove:

            $rmfiles = @("C:\Installers\ImageManager5.0.5.exe",
                         "C:\Installers\ImageManager4.1.exe",
                         "C:\Installers\ImageManager5.exe",
                         "C:\Installers\ImageManagerSetup.exe",
                         "C:\Installers\ShadowProtect_MSP_4.2.7.zip",
                         "C:\Installers\ShadowProtectSetup_MSP_4.2.5.exe",
                         "C:\Installers\VMware-Server-2.0.2-203138.exe",
                         "C:\Installers\control center installer.exe",
                         "C:\Installers\BDR Restore.iso",
                         "C:\Installers\storagecraft.iso")

             foreach ($rmfile in $rmfiles)
                {
                    IF(!(Test-Path $rmfile))
                        { }
                    ELSE
                        {
                            Remove-Item $rmfile -Force -ErrorAction SilentlyContinue
                        }

                }





    }

    function checkforrunningVM

        {

            $CharTecREG = "HKLM:\SOFTWARE\CharTec"

            # Check if Virtual Is running in VirtualBox
                IF((Get-Process VirtualBox -ErrorAction SilentlyContinue) -or (Get-Process vmplayer -ErrorAction SilentlyContinue))

                    {
                        # Virtual Is RUNNING
                            Echo "Virtual Machine is Running"
                            $vmrunning = "1"
                            #Global $vmrunning

                        # Set Alert Flag (Registry Entry) Registry
                            New-Item -path $CharTecREG -name VMRunning -value 1 -Force

                        # Create Alert
                            downloadfile ScheduledTasks/VMRunning.xml c:\BDR\ScheduledTasks\VMRunning.xml
                            SchTasks /create /TN "VMRunning" /xml "C:\BDR\ScheduledTasks\VMRunning.xml"

                    }

                ELSE
                    {
                        # Virtual Machine Not Detected
                            $vmrunning = "0"
                            #Global $vmrunning

                        # Check if Alert Flag is Set (Registry Entry)
                            IF(Test-Path HKLM:\SOFTWARE\CharTec\VMRunning)
                                {
                                    # Flag needs to be removed
                                        Remove-Item HKLM:\SOFTWARE\CharTec\VMRunning -Force
                                    # Remove Alert
                                        SchTasks /delete /TN "VMRunning" /f


                                }
                            ELSE
                                {

                                }


                    }


        }


    Function QSremoval
        {

             # Remove Extra Unlocker software
                    $Users = Get-ChildItem C:\Users -Name

                            # Remove Quickstores shortcut

                        Foreach($User in $Users)
                            {

                                IF(Test-Path "C:\Users\$User\Desktop\Quickstores.lnk")
                                    {
                                        Remove-Item "C:\Users\$User\Desktop\Quickstores.lnk" -Force
                                    }

                                ELSE {}

                            }

                    # Remove Quickstores Icon

                         Foreach($User in $Users)
                            {


                                IF(Test-Path "C:\Users\$User\AppData\Local\Temp\quickstores.ico")
                                    {
                                        Remove-Item "C:\Users\$User\AppData\Local\Temp\quickstores.ico" -Force

                                    }

                                   ELSE {}

                            }
    }


# =================================================================================================================
# BMC Update / Updated ZJ 5-7-2014
# =================================================================================================================

    function bmcupdate

   {

                        #Get BMC Version
                            $bmcver = (Get-Command "C:\BDR\CharTec BDR.exe").FileVersionInfo.ProductVersion

                        #BMC update
                            IF($bmcver -eq "3.6.1")
                                {}
                            Else
                                {
                                 Echo "Updating BMC..."
                                     cd c:\temp
                                     downloadfile /BDR/BMC/CharTecBDR.exe c:\temp\CharTecBDR.exe
                                     Copy-Item C:\Temp\CharTecBDR.exe "C:\BDR\CharTec BDR.exe" -Force



                                 }
    }

# =================================================================================================================
# DecryptKey Update / Updated ZJ 5-7-2014
# =================================================================================================================


    function DecryptKeyUpdate

   {

                        #Get BMC Version
                            $DKvers = (Get-Command "C:\BDR\Production\DecryptKey\KeyDecryptGUI.exe").FileVersionInfo.ProductVersion

                        #BMC update
                            IF($DKvers -eq "1.0.0.0")
                                {}
                            Else
                                {
                                 Echo "Updating DecryptKeyGUI..."
                                     cd c:\temp
                                     downloadfile /DecryptKey/KeyDecryptGUI.exe c:\temp\KeyDecryptGUI.exe
                                     Copy-Item C:\Temp\KeyDecryptGUI.exe C:\BDR\Production\DecryptKey\KeyDecryptGUI.exe -Force



                                 }
    }




# =================================================================================================================
# Vipre
# =================================================================================================================

    function VipreInstall
        {
            # Test for ESET
                if(!(get-service "ekrn.exe" -ErrorAction SilentlyContinue))
                    {
                        # Since ESET Does NOT Exist Check and Install Vipre...
                            if(!(Get-Service "SBAMSvc" -ErrorAction SilentlyContinue))
                                {
                                    Echo "Missing AV, downloading..."
                                        downloadfile Vipre-6.5-2014-08-05.EXE C:\installers\Vipre-6.5-2014-08-05.EXE

                                    Echo "Installing Vipre..."
                                        if(Test-Path 'C:\installers\Vipre-6.5-2014-08-05.EXE')
                                            {
                                                (Start-Process -FilePath "C:\installers\Vipre-6.5-2014-08-05.EXE" -Wait -PassThru).ExitCode
                                            }
                                }
                            else
                                {
                                    #Since Vipre is installed Check Version
                                        $viprever = (Get-Command "C:\Program Files (x86)\GFI Software\GFIAgent\SBAMSvc.exe").FileVersionInfo.ProductVersion

                                    #Check if Vipre is up to date
                                        if($viprever -ne "6.2.5330.0")
                                            {
                                                # Vipre is out of date need to update
                                                    Echo "Downloading Vipre Installer"
                                                        cd C:\temp
                                                        downloadfile Vipre-6.5-2014-08-05.EXE C:\installers\Vipre-6.5-2014-08-05.EXE

                                                    Echo "Installing BMC..."
                                                        if(Test-Path 'C:\installers\Vipre6-26-12.exe')
                                                            {
                                                                (Start-Process -FilePath "C:\installers\Vipre-6.5-2014-08-05.EXE" -Wait -PassThru).ExitCode
                                                            }
                                            }


                                 }
                    }

        }

# =================================================================================================
# PowerShell 3 download / Updated by RL 2/26/2014 /
# =================================================================================================

    function powershell3
        {
            if(!(test-path C:\installers\Windows6.1-KB2506143-x64.msu))
                {
                    $updatelogs += "PowerShell 3.0 Installer [Missing]"
                    downloadfile PowerShell3.0/Windows6.1-KB2506143-x64.msu C:\installers\Windows6.1-KB2506143-x64.msu
                    $updatelogs += "PowerShell 3.0 Installer [Downloading]"

                }
            else
                {
                    $updatelogs += "PowerShell 3.0 Installer [OK]"
                }
             echo "`n`n"
             Add-Content -Path "C:\BDR\Logs\BDRInfo\Update\UpdateScript-$timestamp3.txt" -Value "$updatelogs,"
             return $updatelogs
             echo "`n`n"

        }

# =================================================================================================
# VirtualBox Web Service / Updated by RL 2/26/2014 /
# =================================================================================================

   function installvboxweb
        {
            # 1. Check if Virtualbox Service is installed.
                if(!(Get-Service "VirtualBoxWeb" -ErrorAction SilentlyContinue))
                    {
                        $updatelogs += "VirtualBox Web Service [Missing]"

                        if(Test-Path -Path "C:\installers\nssm.exe")
                            {
                                cd C:\Installers
                                CMD /c nssm.exe install VirtualBoxWeb  "C:\Program Files\Oracle\VirtualBox\VBoxWebSrv.exe"
                                $updatelogs += "VirtualBox Web Service [Installing]"
                            }
                        else
                            {
                                $updatelogs += "VirtualBox Web Service [Missing NSSM]"
                                cd C:\installers
                                downloadfile nssm.exe C:\Installers\nssm.exe
                            }
                    }
                  else
                    {
                        $updatelogs += "VirtualBox Web Service [OK]"
                    }
             echo "`n`n"
             Add-Content -Path "C:\BDR\Logs\BDRInfo\Update\UpdateScript-$timestamp3.txt" -Value "$updatelogs,"
             return $updatelogs
             echo "`n`n"
        }

# =================================================================================================
# Update Virt folder in Apache / Updated by RL 2/26/2014 / * Not sure what the guid thing is for?
# =================================================================================================
    function virt
        {
            if(!(Get-ItemProperty HKLM:\SOFTWARE\CharTec\Virt\4.2))
                {
                    # 1. Download Virt Package
                        downloadfile 'Required_Software_Packages/virt.zip' 'C:\Temp\virt.zip'

                    # 2. Extract
                        if(Test-Path -Path "C:\Temp\virt.zip")
                            {
                                cd "C:\Temp"
                                unzip virt.zip
                                cd "C:\Temp\Virt\"
                                $updatelogs += "Web Update [Downloaded]"

                                # Copy
                                    $virtFrom = "C:\Temp\virt"
                                    $virtTo   = "C:\Apache24\htdocs\Virt"

                                    Copy-Item $virtFrom $virtTo -Force -Recurse
                            }
                 }
              else
                {
                    $updatelogs += "Web Update [OK]"
                }

                  # Test for Existing Reg Key
                    if(test-path HKLM:\SOFTWARE\CharTec\Virt\4.2)
                        {
                            # Get Registry
                                $regkey = (Get-ItemProperty HKLM:\SOFTWARE\CharTec\Virt\4.2).regkey
                        }
                     else
                        {
                            # Generate New Code
                                $guid = [guid]::NewGuid()
                                ($guid).Guid

                            # Insert into Registry
                                New-Item -Path HKLM:\Software\CharTec\ -Name Virt\4.2 –Force
                                New-ItemProperty -Path HKLM:\Software\CharTec\Virt\4.2 -Name regkey -Value $guid -Type String
                        }
             echo "`n`n"
             Add-Content -Path "C:\BDR\Logs\BDRInfo\Update\UpdateScript-$timestamp3.txt" -Value "$updatelogs,"
             return $updatelogs
             echo "`n`n"
        }

# =================================================================================================================
# Install Python / Updated by RL 2/26/2014 /
# =================================================================================================================

       function python
        {
            # Check if already installed
                if(!(Test-Path "C:\Python33" -ErrorAction SilentlyContinue))
                    {
                        echo "`n`n"
                        echo "Installing Python..."
                        echo "`n`n"

                        if(Test-Path "C:\Installers\Python.msi" -ErrorAction SilentlyContinue)
                            {
                                # 1. Install Python
                                    $install = (Start-Process -FilePath "msiexec.exe" -ArgumentList "/i C:\installers\Python.msi /quiet" -Wait -PassThru).ExitCode
                                    if($install -eq 0)
                                        {
                                            $updatelogs += "Python Install [OK]"
                                        }
                                    else
                                        {
                                            $updatelogs += "Python Install [Failed]"
                                        }
                            }
                        else
                            {
                                # 1. Download Python
                                    $updatelogs += "Python Install [Downloaded]"
                                    cd C:\installers
                                    downloadfile python.msi C:\Installers\python.msi
                            }

                        }
                    else
                        {
                            $updatelogs += "Python [OK]"
                        }

             echo "`n`n"
             Add-Content -Path "C:\BDR\Logs\BDRInfo\Update\UpdateScript-$timestamp3.txt" -Value "$updatelogs,"
             return $updatelogs
             echo "`n`n"
        }

# =================================================================================================================
# Install BDRWatchdog Service / Updated RL 3/17/14 /
# =================================================================================================================

   function watchdoginstaller
    {
        if(!(Test-Path C:\Temp\BDRWatchdogServiceSetup.msi))
            {
                echo "Downloading BDRWatchdog Installer"
                        cd C:\Temp
                        downloadfile BDRServiceAgent/BDRAgent/BDRWatchdogServiceSetup.msi C:\Temp\BDRWatchdogServiceSetup.msi
            }
    }




    function BDRwatchdog
        {


            if(!(Get-Service BDRWatchdogService -ErrorAction SilentlyContinue))
                {
                    echo "`n`n"
                    echo "Installing BDR Watchdog Service..."
                    echo "`n`n"

                    # 1. Install BDR Watch Dog Service
                        $install = (Start-Process -FilePath "msiexec.exe" -ArgumentList "/i C:\temp\BDRWatchdogServiceSetup.msi /quiet" -Wait -PassThru).ExitCode

                    # 2. StartWatchDog Service
                        if($install -eq 0)
                            {
                                # Start the newly installed service
                                    sleep -Seconds 2
                                    Start-Service "BDRWatchdogService"
                                    $updatelogs += "BDR Watchdog Service Install [OK]"
                            }
                        else
                            {
                                # Installation was unsuccessful
                                    $updatelogs += "BDR Watchdog Service Install [Failed]"
                            }
                }
            else
                {
                    $updatelogs += "BDR Watchdog Service [OK]"
                }
             echo "`n`n"
             Add-Content -Path "C:\BDR\Logs\BDRInfo\Update\UpdateScript-$timestamp3.txt" -Value "$updatelogs,"
             return $updatelogs
             echo "`n`n"
        }

# =================================================================================================================
# Install BDRService / Updated RL 2/25/14 /
# =================================================================================================================


   function BDRServiceinstaller
    {
        IF(!(Test-Path C:\Temp\BDRServicesetup.msi))
            {
                Echo "Downloading BDRService Installer"
                        cd C:\Temp
                        downloadfile BDRServiceAgent/BDRAgent/BDRServiceSetup.msi C:\Temp\BDRServiceSetup.msi
            }
                Else{}
    }






    function BDRService
        {
            if(!(Get-Service BDRService -ErrorAction SilentlyContinue))
                {
                    echo "`n`n"
                    echo "Installing BDR Agent Service..."
                    echo "`n`n"

                    $install = (Start-Process -FilePath "msiexec.exe" -ArgumentList "/i C:\Temp\BDRServiceSetup.msi /quiet" -Wait -PassThru).ExitCode

                    echo "`n`n"
                    Start-Service BDRService
                    echo "`n`n"

                    if($install -eq 0)
                        {
                            $updatelogs += "BDR Agent Service Install [OK]"
                        }
                    else
                        {
                            $updatelogs += "BDR Agent Service Install [Failed]"
                        }
                }
            else
                {
                    $updatelogs += "BDR Agent Service [OK]"
                }
             echo "`n`n"
             Add-Content -Path "C:\BDR\Logs\BDRInfo\Update\UpdateScript-$timestamp3.txt" -Value "$updatelogs,"
             return $updatelogs
             echo "`n`n"
         }

# =================================================================================================================
# Remove Doyenz Tray Monitor Updated RL /3-10-14/
# =================================================================================================================

    function DoyenzTrayRemoval
        {
            if(Get-Process -processname Doyenz.AgentUpdater.TrayMonitor -ErrorAction SilentlyContinue)
                {
                    $updatelogs += "Doyenz Tray Icon Detected     [Uninstall]"

                    Stop-Process -processname Doyenz.AgentUpdater.TrayMonitor
                    $regkeypath = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Run"
                    $value1     = (Get-ItemProperty $regkeypath).DoyenzTrayMonitor -eq $null

                    if($value1 -eq $False)
                        { Remove-ItemProperty -path HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Run -name DoyenzTrayMonitor }

                    if(Get-Process -processname Doyenz.AgentUpdater.TrayMonitor -ErrorAction SilentlyContinue)
                        { $updatelogs += "Doyenz Tray Icon Uninstall      [Failed]"}
                    else
                        { $updatelogs += "Doyenz Tray Icon Uninstall      [OK]" }
                }

             echo "`n`n"
             Add-Content -Path "C:\BDR\Logs\BDRInfo\Update\UpdateScript-$timestamp3.txt" -Value "$updatelogs,"
             return $updatelogs
             echo "`n`n"

        }

# =================================================================================================================
# Disable and remove Adobe service Updated RL 3/10/14
# =================================================================================================================

    function AdobeService
        {

            # Service Removal
                if(Get-Service AdobeARMservice -ErrorAction SilentlyContinue)
                    {
                        $updatelogs += "Adobe Update Service Uninstall     [Uninstall]"
                        # 1. Stop Service
                            Stop-Service -Name AdobeARMservice -Force
                        # 2. Disable Service
                            Set-Service -Name AdobeARMservice -StartupType Disabled -Status Stopped -ErrorAction SilentlyContinue
                        # 3. Remove Registry Entry
                            Remove-ItemProperty HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Run -Name "Adobe Arm" -ErrorAction SilentlyContinue
                        # 4. Verify Uninstall
                            $adobeservice = Get-Service -Name AdobeARMservice -ErrorAction SilentlyContinue
                            if($adobeservice.Status -eq "Running")
                                { $updatelogs += "Adobe Update Service Uninstall     [Failed]" }
                    }

             # Process Removal
                if(Get-Process -Name AdobeARM -ErrorAction SilentlyContinue)
                    {
                        $updatelogs += "Adobe Update Process Uninstall     [Uninstall]"
                        # 1. Stop Process
                            Stop-Process -Name AdobeARM -Force -ErrorAction SilentlyContinue
                        # 2. Verify Process has stopped.
                            $adobeprocess = Get-Process -Name AdobeARM -ErrorAction SilentlyContinue
                            if($adobeprocess -eq $null)
                                { $updatelogs += "Adobe Update Process Uninstall     [Failed]" }
                    }

             echo "`n`n"
             Add-Content -Path "C:\BDR\Logs\BDRInfo\Update\UpdateScript-$timestamp3.txt" -Value "$updatelogs,"
             return $updatelogs
             echo "`n`n"
        }


# =================================================================================================================
# Install V C++ Redist 2012 -- ZB 8-14-2015
# =================================================================================================================
		function vcredistinstall {
			#Check if installed
			if (!(Test-Path 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\DevDiv\VC\Servicing\11.0\RuntimeMinimum')) {$vcredistinstall = 0} else {$vcredistinstall = 1}
			if ($vcredistinstall -eq 0) {
				if (Test-Path 'C:\PROGRA~1\CharTec\vcredist_x86.exe') {
					echo "Visual C++ 2012 Redist not Detected, Installing..."
					Start-Process -FilePath 'C:\PROGRA~1\CharTec\vcredist_x86.exe' -ArgumentList "/install /quiet /norestart /log C:\PROGRA~1\CharTec\vcredist_inst.log" -Wait
					echo "Visual C++ 2012 Redist Installed!"
				} else {
					echo "uAgent not found, cannot install VC Redist"
				}
			} else {
				echo "Visual C++ 2012 Redist Detected, Not Installing"
			}
		}

# =================================================================================================================
# Remove New SaaS Sched Task -- ZB 9-16-2015 -- Because it's annoying as hell that it doesn't go away properly
# =================================================================================================================
		function remfirstruntask {
			echo "Removing SaaS 'First Run' task..."
			SchTasks /delete /TN "First Run" /f
			SchTasks /delete /TN "SaaS BDR First Run" /f
		}

# =================================================================================================================
# Power Settings -- ZB 9-16-2015 -- Code courtesy of Mike B
# =================================================================================================================
		function setpowerconfig {
			echo "Verifying Power Settings..."
			Powercfg -getactivescheme
			echo "Disk timeout set to 0"
			Powercfg -change -disk-timeout-ac 0
			Powercfg -change -disk-timeout-dc 0
			echo "Standby timeout set to 0"
			Powercfg -change -standby-timeout-ac 0
			Powercfg -change -standby-timeout-dc 0
			echo "Hibernate timeout set to 0"
			Powercfg -change -hibernate-timeout-ac 0
			Powercfg -change -hibernate-timeout-dc 0
		}

# ==================================================================================================================
# Remove Backup Job Scripts (in 'C:\BDR\scripts')
# ==================================================================================================================
	function rembackupscripts {
		echo "Removing backup job scripts..."
		Remove-Item C:\BDR\scripts\*.vbs -Recurse -Force -ErrorAction SilentlyContinue
	}

# ==================================================================================================================
# Install ScreenConnect!
# ==================================================================================================================
	function installScreenConnect {
		echo "Checking for Relyenz ScreenConnect..."
		$testSCInstalled = Get-Service "ScreenConnect Client (54d9f2ec98463e54)"
		if ($testSCInstalled) {
			echo "Relyenz ScreenConnect already present!"
		} else {
			Start-Process -FilePath 'msiexec' -ArgumentList '/i C:\Installers\ScreenConnect.ClientSetup-ClassicBDR.msi /quiet /qn /norestart' -Wait
			echo "ScreenConnect Installed!"
		}
	}

# ==================================================================================================================
# Backup Share Protection
# ==================================================================================================================

function backupShareProtection {
  $memberid = Get-Content c:\BDR\serial.txt -ea SilentlyContinue;

  $acceptedMembers = "10019055", "10018791"; # an array of strings containing valid member ids
  $shareNames = 'Backup', 'Backup2';

  function enactShareProtection($share) {
    Grant-SmbShareAccess -Name $share -AccountName 'Everyone' -AccessRight Read -Force | Out-Null;
    Grant-SmbShareAccess -Name $share -AccountName 'arrcnetadmin' -AccessRight Full -Force | Out-Null;
    Grant-SmbShareAccess -Name $share -AccountName 'BUILTIN\Administrators' -AccessRight Full -Force | Out-Null;
  }

  function checkIfShareExists($share) {
    $test = Get-SmbShare -Name $share -ea SilentlyContinue;
    if ($test) {
      return $true;
    } else {
      return $false;
    }
  }

  if ($acceptedMembers -contains $memberid) {
    foreach ($shareName in $shareNames) {
      $check = checkIfShareExists $shareName;
      if ($check) {
        enactShareProtection $shareName;
        Get-SmbShareAccess -Name $shareName;
      }
    }
  }
}

# =================================================================================================================
#
#                                  Installation  ( running the functions )
#
# =================================================================================================================

    Echo "Creating / Monitoring File Structure..."
        createdirectories

    Echo "Checking for Running Virtual Machines..."
        checkforrunningVM

    Echo "Creating / Monitoring Scheduled Tasks..."
        scheduledtasks

    # ARRC BDRs only:
        ARRCDNS

    # CharTec BDRs only:
        removelabtech

    Echo "Creating / Monitoring Update Scripts..."
        updatescript

    Echo "Removing Plainkey"
        removeplainkey

    Echo "Update / Monitor HSR Monitor..."
        updatehsr

    Echo "Update / Monitor ImageVerification..."
        updateiv

    Echo "Update / Monitor C:\BDR Directory... Including BMC Software"
        updatebdrdirectory

    Echo "Update / Monitor Remote Access Agents (TeamViewer and LogMeIn)..."
        remoteaccess

    Echo "Update / Monitor Documents..."
        documents

    Echo "Install Fixes..."
        fixes

    Echo "Update / Monitor C:\Installer Directory..."
        updateinstallerdirectory

  #  Echo "Update / Monitor / Install Web Interfaces..."
  #    webbmc

    Echo "Checking MySQL - Update / Monitor..."
        mysql

    Echo "Checking StorageCraft Software..."
        updatebdrshadowprotect

    # Vmware Software Removal
        vmwareremoval

    # Disable Firewall
        disablefirewall

    # PHP related scripts
        php

    # Disable HSR

        IF(!(Test-Path HKLM:SOFTWARE\CharTec\rmhsr1))
            {
                disablehsr
                $CharTecREG = "HKLM:\SOFTWARE\CharTec"
                New-Item -path $CharTecREG -name rmhsr1 -value 1 -Force

            }

     # Clean up C:\Installers DIR
        cleaninstallerdir


     # BMC Update
        bmcupdate

     # DecryptKey Update
     DecryptKeyUpdate

     # Vipre
        VipreInstall

    # Powershell 3.0 Download
        powershell3

    # VirtualBox Web Service
    #    installvboxweb

    #virt
        virt

     #QSRemoval
         QSremoval

  #watchdog installer
        watchdoginstaller

    #BDR service installer
        BDRServiceinstaller

    #BDRservice
        BDRService

    #BDRwatchdog
        BDRwatchdog

    #python
        python

    #doyenztrayremoval
        DoyenzTrayRemoval

    #AdobeService
        AdobeService

	#V C++ Redist 2012
		vcredistinstall

	# Power configuration
		setpowerconfig

	# Remove "First Run" Task
		remfirstruntask

	# Remove BDR folder scripts
		rembackupscripts

	# Install ScreenConnect
		installScreenConnect

  # Backup Share Protection
    backupShareProtection
